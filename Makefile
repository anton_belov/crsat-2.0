##############################################################################
#

# setup and flags
#
CC=gcc

CFLAGS = -m64 -Wall
CFLAGS += -O3
#CFLAGS += -g 
#CFLAGS += -pg
#CFLAGS =  -std=c++0x
#CFLAGS += -Wno-deprecated -Wno-write-strings -fno-strict-aliasing 
#CFLAGS += -DNDEBUG
#CFLAGS += -DNOSTATS
CFLAGS += -DTRACE=0

# if st=1 is given and we're on Linux - link statically
ifeq ($(st), 1)
ifeq ($(findstring Linux, $(shell uname)), Linux)
  CFLAGS += -static
endif
endif

# main executable name
#
TARGET=crsat

# list of sources
#
C_SRCS=aiger.c

CC_SRCS=crsat.cc \
        circuit.cc \
        bcp.cc \
        param.cc \
        stats.cc \
        tracer.cc \
        utils.cc \
        gzstream.cc

OBJS=$(C_SRCS:.c=.o) $(CC_SRCS:.cc=.o)

# list of includes
#
INCLUDES=-I.

##############################################################################

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(TARGET) -lstdc++ -lm -lz

%.o: %.c
	$(CC) $(INCLUDES) $(CFLAGS) -c $<

%.o: %.cc	
	$(CC) $(INCLUDES) $(CFLAGS) -c $<

clean:	
	rm -f $(OBJS) $(TARGET)

backup:
	zip backup.zip *.c *.h *.cc Makefile

depend.mk: 
	# making dependencies
	$(CC) $(INCLUDES) $(CFLAGS) -MM $(CC_SRCS) $(C_SRCS) > depend.mk

-include depend.mk
