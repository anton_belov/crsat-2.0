// ----------------------------------------------------------------------------
//
// bcp.cc - implementations of circuit-based BCP
//
// Anton Belov, antonb@cse.yorku.ca, May 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//
// There are three sets of propagation tables in this file. The first set
// (ptable1_... tables) is designed for constraint propagation in an unassigned
// circuit; the second set (ptable2_... tables) is designed for propagation to
// children in an assigned circuit; the third set (ptable3_... tables) is 
// designed for propagation to parents and siblings in an assigned circuit.
//
// 1. The first set of tables (for unassigned circuits) is constructed in the 
//    following way: for an n-ary gate the index is the trenary representation 
//    of the sequence
//                             in_{n-1} ... in_0 out
//    which captures the current state of the gate. Here, each value is either
//    0 (for false), 1 (for true), 2 (for unknown). The value is a pair of the
//    next state (encoded in the same way), and the action. The constants for 
//    actions are defined in circuit.h and designed to be OR'ed (except for
//    CONFLICT and NOOP)
//
// 2. The second set of tables (for propagation to children in assigned 
//    circuits) are constructed similarly, except the representation is binary 
//    (0/1 for false/true respectively). 
//
// 3. The third set of tables (for propagation to parents and siblings in 
//    assigned circuits) is also binary - however since in some situations
//    there's a choice between propagation to sibling vs propagation to
//    parent, these tables contain a second (alternative) set of actions.

#include "circuit.h"

//
// -----------------------  Globally available data ---------------------------
//

//
// ---------------------------  Local declarations ----------------------------
//
namespace {


// ---- The first set of tables (propagation in unassigned circuit) starts ----

// ptable1 for IN gates
//
int ptable1_in[][2] = {     // current_state   next_state      action
                            // out (idx)       out (idx)
                            // ------------------------------------------------
    { 0, PF },              // 0   ( 0)        0   ( 0)        PF
    { 1, PF },              // 1   ( 1)        1   ( 1)        PF
    { 2, NOOP }             // 2   ( 2)        2   ( 2)        NOOP
};

// ptable1 for OUT gates
//
int ptable1_out[][2] = {    //  current_state   next_state     action
                            //  in0 out (idx)   in0 out (idx)
                            //  -----------------------------------------------
    { 0, NOOP },            //  0   0   ( 0)    0   0   ( 0)   NOOP
    { 1, CONFLICT },        //  0   1   ( 1)    0   1   ( 1)   CONFLICT
    { 0, PF },              //  0   2   ( 2)    0   0   ( 0)   PF
    { 3, CONFLICT },        //  1   0   ( 3)    1   0   ( 3)   CONFLICT
    { 4, NOOP },            //  1   1   ( 4)    1   1   ( 4)   NOOP
    { 4, PF },              //  1   2   ( 5)    1   1   ( 4)   PF
    { 0, PC0 },             //  2   0   ( 6)    0   0   ( 0)   PC0
    { 4, PC0 },             //  2   1   ( 7)    1   1   ( 4)   PC0
    { 8, NOOP },            //  2   2   ( 8)    2   2   ( 8)   NOOP
};

// ptable1 for AND2 gates
//
int ptable1_and2[][2] = {   //  current_state       next_state         action
                            //  in1 in0 out (idx)   in1 in0 out (idx)
                            //  -----------------------------------------------
    { 0, NOOP },            //  0   0   0   ( 0)    0   0   0   ( 0)   NOOP
    { 1, CONFLICT },        //  0   0   1   ( 1)    0   0   1   ( 1)   CONFLICT
    { 0, PF },              //  0   0   2   ( 2)    0   0   0   ( 0)   PF
    { 3, NOOP },            //  0   1   0   ( 3)    0   1   0   ( 3)   NOOP
    { 4, CONFLICT },        //  0   1   1   ( 4)    0   1   1   ( 4)   CONFLICT
    { 3, PF },              //  0   1   2   ( 5)    0   1   0   ( 3)   PF
    { 6, NOOP },            //  0   2   0   ( 6)    0   2   0   ( 6)   NOOP
    { 7, CONFLICT },        //  0   2   1   ( 7)    0   2   1   ( 7)   CONFLICT
    { 6, PF },              //  0   2   2   ( 8)    0   2   0   ( 6)   PF
    { 9, NOOP },            //  1   0   0   ( 9)    1   0   0   ( 9)   NOOP
    {10, CONFLICT },        //  1   0   1   (10)    1   0   1   (10)   CONFLICT
    { 9, PF },              //  1   0   2   (11)    1   0   0   ( 9)   PF
    {12, CONFLICT },        //  1   1   0   (12)    1   1   0   (12)   CONFLICT
    {13, NOOP },            //  1   1   1   (13)    1   1   1   (13)   NOOP
    {13, PF },              //  1   1   2   (14)    1   1   1   (13)   PF
    { 9, PC0 },             //  1   2   0   (15)    1   0   0   ( 9)   PC0
    {13, PC0 },             //  1   2   1   (16)    1   1   1   (13)   PC0
    {17, NOOP },            //  1   2   2   (17)    1   2   2   (17)   NOOP
    {18, NOOP },            //  2   0   0   (18)    2   0   0   (18)   NOOP
    {19, CONFLICT },        //  2   0   1   (19)    2   0   1   (19)   CONFLICT
    {18, PF },              //  2   0   2   (20)    2   0   0   (18)   PF
    { 3, PC1 },             //  2   1   0   (21)    0   1   0   ( 3)   PC1
    {13, PC1 },             //  2   1   1   (22)    1   1   1   (13)   PC1
    {23, NOOP },            //  2   1   2   (23)    2   1   2   (23)   NOOP
    {24, NOOP },            //  2   2   0   (24)    2   2   0   (24)   NOOP
    {13, PC0|PC1 },         //  2   2   1   (25)    1   1   1   (13)   PC0, PC1
    {26, NOOP }             //  2   2   2   (26)    2   2   2   (26)   NOOP
};


// -- The second set of tables (propagation to children in assigned circuit) --
// --                                  starts                                --

// ptable2 for IN gates - not applicable (no children)
//

// ptable2 for OUT gates
//
int ptable2_out[][2] = {    //  current_state   next_state     action
                            //  in0 out (idx)   in0 out (idx)
                            //  -----------------------------------------------
    { 0, NOOP },            //  0   0   ( 0)    0   0   ( 0)   NOOP
    { 3, PC0 },             //  0   1   ( 1)    1   1   ( 3)   PC0
    { 0, PC0 },             //  1   0   ( 2)    0   0   ( 0)   PC0
    { 3, NOOP }             //  1   1   ( 3)    1   1   ( 3)   NOOP
};

// ptable2 for AND2 gates; note for case 6 - this could be a triple case-split
//
int ptable2_and2[][2] = {   //  current_state       next_state         action
                            //  in1 in0 out (idx)   in1 in0 out (idx)
                            //  -----------------------------------------------
    { 0, NOOP },            //  0   0   0   ( 0)    0   0   0   ( 0)   NOOP
    { 7, PC0|PC1 },         //  0   0   1   ( 1)    1   1   1   ( 7)   PC0, PC1
    { 2, NOOP },            //  0   1   0   ( 2)    0   1   0   ( 2)   NOOP
    { 7, PC1 },             //  0   1   1   ( 3)    1   1   1   ( 7)   PC1
    { 4, NOOP },            //  1   0   0   ( 4)    1   0   0   ( 4)   NOOP
    { 7, PC0 },             //  1   0   1   ( 5)    1   1   1   ( 7)   PC0
    { 6, NOOP },            //  1   1   0   ( 6)    1   1   0   ( 6)   NOOP
    { 7, NOOP }             //  1   1   1   ( 7)    1   1   1   ( 7)   NOOP
};

// --   The third set of tables (propagation to parents and siblings in      -- 
// --                assigned circuit) starts                                --
// In these tables: th = "this"; oth = "other"; PF = "propagate to parent"; 
//                  PO = "propagate to other"
// Note that the gate type is the type of the parent

// ptable3 for IN gates - not applicable (a parent cannot be IN gate)
//

// ptable3 for OUT gates
//
int ptable3_out[][2] = {    //  current_state   next_state     action
                            //  th  par (idx)   th  par (idx)
                            //  -----------------------------------------------
    { 0, NOOP },            //  0   0   ( 0)    0   0   ( 0)   NOOP
    { 0, PF },              //  0   1   ( 1)    0   0   ( 0)   PF
    { 3, PF },              //  1   0   ( 2)    1   1   ( 3)   PF
    { 3, NOOP }             //  1   1   ( 3)    1   1   ( 3)   NOOP
};

// ptable3 for AND2 gates
//
int ptable3_and2[][2] = {   //  current_state       next_state         action
                            //  oth th  par (idx)   oth th  par (idx)
                            //  -----------------------------------------------
    { 0, NOOP },            //  0   0   0   ( 0)    0   0   0   ( 0)   NOOP
    { 0, PF },              //  0   0   1   ( 1)    0   0   0   ( 0)   PF
    { 2, PO },              //  0   1   0   ( 2)    0   1   0   ( 2)   PO
    { 7, PO },              //  0   1   1   ( 3)    1   1   1   ( 7)   PO
    { 4, NOOP },            //  1   0   0   ( 4)    1   0   0   ( 4)   NOOP
    { 4, PF },              //  1   0   1   ( 5)    1   0   0   ( 4)   PF
    { 2, PO },              //  1   1   0   ( 6)    0   1   0   ( 2)   PO
    { 7, NOOP }             //  1   1   1   ( 7)    1   1   1   ( 7)   NOOP
};    

// ptable3a for AND2 gates - the alternatives table, NOOP means "no 
// alternative"
//
int ptable3a_and2[][2] = {  //  current_state       next_state         action
                            //  oth th  par (idx)   oth th  par (idx)
                            //  -----------------------------------------------
    { 0, NOOP },            //  0   0   0   ( 0)    0   0   0   ( 0)   NOOP
    { 1, NOOP },            //  0   0   1   ( 1)    0   0   1   ( 1)   NOOP
    { 2, NOOP },            //  0   1   0   ( 2)    0   1   0   ( 2)   NOOP
    { 2, PF },              //  0   1   1   ( 3)    0   1   0   ( 2)   PF
    { 4, NOOP },            //  1   0   0   ( 4)    1   0   0   ( 4)   NOOP
    { 5, NOOP },            //  1   0   1   ( 5)    1   0   1   ( 5)   NOOP
    { 7, PF },              //  1   1   0   ( 6)    1   1   1   ( 7)   PF
    { 7, NOOP }             //  1   1   1   ( 7)    1   1   1   ( 7)   NOOP
};

// functions for encoding/decoding indexes in trenary-based tables:
//   tren_encode maps sets of truth-values to index; tren_decode does the 
//   reverse
inline int trenary_encode0(tvs out) { return out; }
inline void trenary_decode0(int val, tvs& out) { out = (tvs)val; }
inline int trenary_encode1(tvs in0, tvs out) { return in0*3+out; }
inline void trenary_decode1(int val, tvs& in0, tvs& out) 
{ in0 = (tvs)(val/3); out = (tvs)(val%3); }
inline int trenary_encode2(tvs in1, tvs in0, tvs out) 
{ return in1*9+in0*3+out; }
inline void trenary_decode2(int val, tvs& in1, tvs& in0, tvs& out) 
{ in1 = (tvs)(val/9); in0 = (tvs)((val-in1*9)/3); out = (tvs)(val%3); }

// functions for encoding/decoding indexes in binary-based tables:
//   tren_encode maps sets of truth-values to index; tren_decode does the 
//   reverse
inline int binary_encode0(tvs out) { return out; }
inline void binary_decode0(int val, tvs& out) { out = (tvs)val; }
inline int binary_encode1(tvs in0, tvs out) { return (in0<<1)|out; }
inline void binary_decode1(int val, tvs& in0, tvs& out) 
{ in0 = (tvs)((val&2)>>1); out = (tvs)(val&1); }
inline int binary_encode2(tvs in1, tvs in0, tvs out) 
{ return (in1<<2)|(in0<<1)|out; }
inline void binary_decode2(int val, tvs& in1, tvs& in0, tvs& out) 
{ in1 = (tvs)((val&4)>>2); in0 = (tvs)((val&2)>>1); out = (tvs)(val&1); }

}


//
// ------------------------  Exported implementations -----------------------
//

// Given a gate type, and the truth-values of inputs and outputs (unused inputs
// are ignored), returns the appropriate BCP action - the actions are taken 
// from the table for propagation in unassigned circuits (ptable1); as a 
// side effect, the values of inputs/outputs are set to the appropriate next 
// state.
bcp_actions lookup_bcp1_action(gate::types t, tvs& in1, tvs& in0, tvs& out)
{
  int idx;
  switch (t) {
  case gate::IN:
    idx = trenary_encode0(out);
    trenary_decode0(ptable1_in[idx][0], out);
    return (bcp_actions)ptable1_in[idx][1];
  case gate::OUT:
    idx = trenary_encode1(in0, out);
    trenary_decode1(ptable1_out[idx][0], in0, out);
    return (bcp_actions)ptable1_out[idx][1];
  case gate::AND:
    idx = trenary_encode2(in1, in0, out);
    trenary_decode2(ptable1_and2[idx][0], in1, in0, out);
    return (bcp_actions)ptable1_and2[idx][1];
  }
  assert(false);
}

// Given a gate type, and the truth-values of inputs and outputs (unused inputs
// are ignored), returns the appropriate BCP action - the actions are taken 
// from the table for propagation to children in assigned circuits (ptable2); 
// as a side effect, the values of inputs/outputs are set to the appropriate 
// next state.
bcp_actions lookup_bcp2_action(gate::types t, tvs& in1, tvs& in0, tvs& out)
{
  int idx;
  switch (t) {
  case gate::IN:
    // INs do not have children
    return NOOP;
  case gate::OUT:
    idx = binary_encode1(in0, out);
    binary_decode1(ptable2_out[idx][0], in0, out);
    return (bcp_actions)ptable2_out[idx][1];
  case gate::AND:
    idx = binary_encode2(in1, in0, out);
    binary_decode2(ptable2_and2[idx][0], in1, in0, out);
    return (bcp_actions)ptable2_and2[idx][1];
  }
  assert(false);
}

// Given a gate type, and the truth-values of this, the other and the parent
// nodes (unused are ignored), returns the appropriate BCP action - the actions 
// are taken from the table for propagation to parents and siblings in assigned 
// circuits (ptable3); as a side effect, the values are set to the appropriate 
// next state.
bcp_actions lookup_bcp3_action(gate::types t, tvs& oth, tvs& th, tvs& par)
{
  int idx;
  switch (t) {
  case gate::IN:
    // INs cannot be parents
    return NOOP;
  case gate::OUT:
    idx = binary_encode1(th, par);
    binary_decode1(ptable3_out[idx][0], th, par);
    return (bcp_actions)ptable3_out[idx][1];
  case gate::AND:
    idx = binary_encode2(oth, th, par);
    binary_decode2(ptable3_and2[idx][0], oth, th, par);
    return (bcp_actions)ptable3_and2[idx][1];
  }
  assert(false);
}

// Returns alternatives to actions returned by lookup_bcp3_action; semantics
// as the same as lookup_bcp3_action, except NOOP means "no alternative".
bcp_actions lookup_bcp3a_action(gate::types t, tvs& oth, tvs& th, tvs& par)
{
  int idx;
  switch (t) {
  case gate::IN:
  case gate::OUT:
    return NOOP;
  case gate::AND:
    idx = binary_encode2(oth, th, par);
    binary_decode2(ptable3a_and2[idx][0], oth, th, par);
    return (bcp_actions)ptable3a_and2[idx][1];
  }
  assert(false);
}
