/*
 Copyright (c) 2009-2011, Anton Belov, anton.belov@ucd.ie
 */

// ----------------------------------------------------------------------------
//
// circuit.cc - implementations of circuit functionality
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <stdexcept>
#include <sstream>
#include <zlib.h>
#include "aiger.h"
#include "circuit.h"
#include "gzstream.h"
#include "param.h"
#include "stats.h"
#include "tracer.h"
#include "utils.h"

using namespace std;

//
// -----------------------  Globally available data ------------------------
//

// the owning circuit
circuit* jfront_cmp::_c = NULL;

//
// ---------------------------  Local declarations --------------------------
//
namespace {
}

//
// ------------------------  Exported implementations -----------------------
//

// Returns true when this gate is structurally the same g
bool gate::operator==(const gate& g) const
{
  return (_id == g._id) &&
         (_type == g._type) &&
         (_constrained == g._constrained) &&
         (_depth == g._depth) &&
         (_left_id == g._left_id) &&
         (_right_id == g._right_id) &&
         (_num_parents == g._num_parents) &&
         (memcmp(parent_ids(), g.parent_ids(), _num_parents * sizeof(int)) == 0);
}

// Returns true when the (try) state of this gate is the same as g
bool gate::same_state(const gate& g, long tr) const
{
  return (tv(tr) == g.tv(tr)) &&
         (just(tr) == g.just(tr)) &&
         (bcp_tv() == g.bcp_tv());
}

// output operator for gates
ostream& operator<<(ostream& out, const gate& g)
{
  out << "(id=" << g.id()
      << ", type=" << g.type()
      << ", constrained=" << g.constrained()
      << ", depth=" << g.depth()
      << ", left=" << g.left_id()
      << ", right=" << g.right_id()
      << ", num_parents=" << g.num_parents()
      << ", tv=" << g.tv()
      << ", just=" << g.just()
      << ", bcp_tv=" << g.bcp_tv()
      << ", tr_tv=" << g.tv(g._tr_tv_gen) << "@" << g._tr_tv_gen
      << ", tr_just=" << g.just(g._tr_just_gen) << "@" << g._tr_just_gen
      << ")";
  return out;
}

// Reads the circuit description from the specifed file, and creates
// the corresponding circuit.
circuit* circuit::create(string& in_file)
{
  // determine the extension, and kick off the proper parsing function
  size_t p = in_file.find_last_of('.');
  if (p == string::npos)
    throw invalid_argument("circuit::create(): unable to determine the input "
        "file type; only .crs and .aig files (possibily .gz compressed) are "
        "supported.");
  string ext = in_file.substr(p);
  bool compressed = false;
  if (ext == ".gz") {
    compressed = true;
    size_t next_p = string::npos;
    if (p > 0)
      next_p = in_file.find_last_of('.', p-1);
    if (next_p == string::npos)
      throw invalid_argument("circuit::create(): invalid input file name: "
          "compressed files must have .crs.gz or .aig.gz extension");
    ext = in_file.substr(next_p, (p - next_p));
  }
  // kick off the actual readers to create the circuit
  circuit* c = NULL;
  if (ext == ".aig") {
    void *in = (compressed)
        ? (void*)(gzopen(in_file.c_str(), "r"))
        : (void*)(fopen(in_file.c_str(), "r"));
    if (in == NULL)
      throw invalid_argument("circuit::create(): unable to open input file.");
    c = create_from_aig(in,
        (compressed) ? (aiger_get)gzgetc : (aiger_get)fgetc);
    if (compressed)
      gzclose((gzFile)in);
    else
      fclose((FILE*)in);
  } else {
    throw invalid_argument("circuit::create(): input file name has an unsupported"
      " extension; only .crs and .aig files are supported.");
  }
  assert(c != NULL);
  // do some calculations of the static properties of the gates (note that some
  // of the static properties must be calculated only after all constraints
  // have been propagated)
  return c;
}

// Reads the circuit description from an AIGER source. The parameters are the
// same as the last two parameters of aiger_read_generic: i.e. a void pointer
// to (probably) file input stream, and a getter function with the signature
// of int (*) (void *). The first parameter will be used as input to the
// getter. For example, for reading from regular files, set 'in' to be the
// FILE* obtained from fopen(), and 'getter' to be fgetc.
circuit* circuit::create_from_aig(void* in, aiger_get getter)
{
  // initialize AIGER library
  aiger *agr = aiger_init ();
  // read and parse the input
  const char *err = aiger_read_generic(agr, in, getter);
  if (err)
    throw invalid_argument("circuit::create_from_aig(): error parsing input.");
  if ((agr->num_latches != 0) || (agr->num_outputs != 1))
    throw invalid_argument("circuit::create_from_aig(): only combinational "
        "circuits (0 latches, 1 output) are supported.");

  // circuit parameters ...
  int num_gates = agr->num_inputs + agr->num_ands + 1; // 1 is for output
  int num_outs = 1;
  int num_ins = agr->num_inputs;
  int num_wires = 2 * agr->num_ands + num_outs;
  int num_constr = 1;
  // offset will be used to map the AIGER id's to ours -- in AIGER the numbering
  // goes from inputs to outputs; in our case its the opposite; but since AIGER
  // binary is in topological order (id_parent > id_child), the mapped version
  // will also be in topological order
  int offset = num_gates + 1;

  // ok, allocate the memory
  circuit* c = NULL;
  // allocate memory; the memory will look like this:
  // | circuit | indexes of constraints | array of pointers to gates | gate_0 | parents_0 | gate_1 | parents_1 | ... |
  void* mem = malloc(sizeof(circuit) +
    (num_gates + 1) * sizeof(gate) +  // for gates (0-th gate is unused)
    num_wires * sizeof(int) +         // for wires (i.e. parents)
    (num_gates + 1) * sizeof(gate*) + // for storage inside gate_array's
    num_constr * sizeof(int));        // for constraints
  if (mem == NULL)
    throw runtime_error("circuit:create_from_aig(): not enough memory "
        "available to store the circuit.");
  c = new (mem) circuit();
  c->_mem = mem;
  c->_num_gates = num_gates;
  c->_num_outs = num_outs;
  c->_num_ins = num_ins;
  c->_num_constrs = num_constr;
  c->_jfront.init(num_gates+1);
  c->_jfront.comparator().set_circuit(c);
  c->_objective = 0;
  c->_gate_queue.init(num_gates+1);
  c->_gate_queue2.init(num_gates+1);
  c->_gate_pqueue.init(num_gates+1);
  c->_gate_pqueue2.init(num_gates+1);
  c->_tr_gen = 0;
  c->_tr_objective = 0;
  c->_depth = 0;
  c->_num_unreach = 0;
  // layout arrays
  c->_constrs = (int*)((unsigned char*)mem + sizeof(circuit));
  // mem will now point to the beginning of gate array storage
  mem = c->_constrs + c->_num_constrs;
  c->_gates._storage = (gate**)mem;
  c->_outs._storage = c->_gates._storage + 1;
  c->_ins._storage = c->_gates._storage + c->_num_gates + 1 - c->_num_ins;
  mem = c->_gates._storage + c->_num_gates + 1;   // mem points to the begining of gate storage
  // 0-th gate is unused
  new (mem) gate(0, (gate::types)0, 0, 0, 0);
  c->_gates._storage[0] = (gate*)mem;
  // mem will now point to the begining of the first gate
  mem = (unsigned char*)mem + sizeof(gate);
  // 1-st gate is going to be the output - we will constrain it to 1, and
  // connect it to the one and only output of AIGER (mapped): should be 2 or -2
  unsigned int out_lit = agr->outputs[0].lit;
  new (mem) gate(1, gate::OUT,
      (aiger_sign(out_lit) ? -1 : 1) * (offset - aiger_lit2var(out_lit)),
      0, 0);
  c->_gates._storage[1] = (gate*)mem;
  assert(abs(((gate*)mem)->left_id()) == 2);
  // mem will now point to the begining of gate 2 (first AND)
  mem = (unsigned char*)mem + sizeof(gate);
  // in order to build a parent's list for each gate we keep a map from gate id
  // to its parents, and as we traverse the circuit in topological order, for
  // each gate we add its id to the map entries of its children. Since the
  // circuit is in topological order, by the time we hit a gate, all of its
  // parents has already been processed, and therefore its map entry will have
  // all its parents listed
  map< int, vector<int> > p_map;
  p_map[2].push_back(aiger_sign(out_lit) ? -1 : 1); // the only parent of 2
  // go ...
  for (int i = agr->num_ands - 1; i >= 0; i--) {
      aiger_and& and_gate = agr->ands[i];
      int id = offset - aiger_lit2var(and_gate.lhs); // new id
      int left = offset - aiger_lit2var(and_gate.rhs0);
      if (aiger_sign(and_gate.rhs0))
        left = -left;
      int right = offset - aiger_lit2var(and_gate.rhs1);
      if (aiger_sign(and_gate.rhs1))
        right = -right;
      if (!p_map.count(id)) { // must have at least one parent !
        cout << "Warning: the circuit contains unreachable gates. This should "
                "not happen in binary AIG." << endl;
        assert(false);
      }
      new (mem) gate(id, gate::AND, left, right, p_map[id].size());
      c->_gates._storage[id] = (gate*)mem;
      mem = (unsigned char*)mem + sizeof(gate);
      // write out parents ...
      for (vector<int>::iterator iter = p_map[id].begin();
          iter != p_map[id].end(); ++iter) {
        *(int*)mem = *iter;
        mem = (int*)mem + 1;
      }
      p_map.erase(id);
      // update the map entries for children
      p_map[abs(left)].push_back(((left < 0) ? -1 : 1) * id);
      p_map[abs(right)].push_back(((right < 0) ? -1 : 1) * id);
      // done with this gate
  }
  // ok, done with AND's, create the inputs ...
  for (int i = agr->num_inputs; i >= 1; i--) {
    int id = offset - i; // new id
    if (!p_map.count(id)) { // must have at least one parent !
      cout << "Warning: the circuit contains unreachable gates. This should "
              "not happen in binary AIG." << endl;
      assert(false);
    }
    new (mem) gate(id, gate::IN, 0, 0, p_map[id].size());
    c->_gates._storage[id] = (gate*)mem;
    mem = (unsigned char*)mem + sizeof(gate);
    // write out parents ...
    for (vector<int>::iterator iter = p_map[id].begin();
        iter != p_map[id].end(); ++iter) {
      *(int*)mem = *iter;
      mem = (int*)mem + 1;
    }
    p_map.erase(id);
    // done with this gate
  }
  // and, finally, the one and only constrained gate (output)
  c->_constrs[0] = 1;
  // done
  aiger_reset(agr);
  return c;
}

// Frees up the memory and any other resources allocated for a circuit
void circuit::destroy(circuit* c)
{
  c->_jfront.~int_pqueue();
  c->_gate_queue.~int_queue();
  c->_gate_queue2.~int_queue();
  c->_gate_pqueue.~int_pqueue();
  c->_gate_pqueue2.~int_pqueue();
  c->_constr_gates.~vector();
  c->_jcache.~map();
  free(c->_mem);
}

// Makes and returns a copy of a circuit
circuit* circuit::copy(circuit& from)
{
  // get the memory - the number of bytes can be obtained by looking at the end of
  // the from circuit
  long num_bytes = (long)&from._gates[from._num_gates] +
                  sizeof(gate) +
                  from._gates[from._num_gates].num_parents() * sizeof(int) -
                  (long)from._mem;
  void* mem = malloc(num_bytes);
  if (mem == NULL)
    throw runtime_error("circuit::create(): not enough memory available to store the circuit.");
  circuit& to = *(new (mem) circuit());
  to._mem = mem;
  // copy all the copiable first
  to._num_gates = from._num_gates;
  to._num_ins = from._num_ins;
  to._num_outs = from._num_outs;
  to._num_constrs = from._num_constrs;
  to._jfront = from._jfront;
  to._objective = from._objective;
  to._gate_queue = from._gate_queue;
  to._gate_queue2 = from._gate_queue2;
  to._gate_pqueue = from._gate_pqueue;
  to._gate_pqueue2 = from._gate_pqueue2;
  to._constr_gates = from._constr_gates;
  to._tr_gen = from._tr_gen;
  to._tr_objective = from._tr_objective;
  to._depth = from._depth;
  to._num_unreach = from._num_unreach;

  // copy the original constraints
  to._constrs = (int*)((unsigned char*)mem + sizeof(circuit));
  memcpy(to._constrs, from._constrs, to._num_constrs * sizeof(int));
  // gate array storage - we will need to fix all the pointers
  to._gates._storage = (gate**)(to._constrs + to._num_constrs);
  to._outs._storage = to._gates._storage + 1;
  to._ins._storage = to._gates._storage + to._num_gates + 1 - to._num_ins;
  // to_mem and from_mem will now point to the begining of gate storage
  void *to_mem = to._gates._storage + to._num_gates + 1;
  void *from_mem = from._gates._storage + from._num_gates + 1;
  for (int i = 0; i <= from._num_gates; i++)
    to._gates._storage[i] = (gate*)((unsigned char*)to_mem + ((long)from._gates._storage[i] - (long)from_mem));
  // the gates themselves can be copied in one shot
  memcpy(to_mem, from_mem,
         num_bytes - sizeof(circuit) - from._num_constrs*sizeof(int) - (from._num_gates + 1)*sizeof(gate*));
  return &to;
}

// Returns true when this circuit is structurally the same c
bool circuit::operator==(const circuit& c) const
{
  if ((_num_gates == c._num_gates) &&
      (_num_ins == c._num_ins) &&
      (_num_outs == c._num_outs) &&
      (_num_constrs == c._num_constrs) &&
      (_depth == c._depth) &&
      (_num_unreach == c._num_unreach) &&
      (memcmp(_constrs, c._constrs, _num_constrs * sizeof(int)) == 0)) {
    for (int i = 0; i <= _num_gates; i++)
      if (!(_gates[i] == c._gates[i]))
        return false;
    return true;
  }
  return false;
}

// Returns true when the state of this circuit is the same as c; assumes
// that this circuit is structurally the same as c; note - trial values are
// not checked
bool circuit::same_state(const circuit& c) const
{
  assert(*this == c);
  if ((_jfront == c._jfront) &&
      (_objective == c._objective) &&
      (_gate_queue == c._gate_queue) &&
      (_gate_queue2 == c._gate_queue2) &&
      (_gate_pqueue == c._gate_pqueue) &&
      (_constr_gates == c._constr_gates)) {
    for (int i = 0; i <= _num_gates; i++)
      if (!_gates[i].same_state(c._gates[i]))
        return false;
    return true;
  }
  return false;
}

// Prints out the circuit to the given output stream (for debugging)
ostream& operator<<(ostream& out, const circuit& c)
{
  out << "Circuit: _num_gates=" << c._num_gates
      << ", _num_ins=" << c._num_ins
      << ", _num_outs=" << c._num_outs
      << ", _num_constr=" << c._num_constrs
      << ", _jfront size=" << c._jfront.size()
      << ", _objective=" << c._objective
      << ", _tr_gen=" << c._tr_gen
      << ", _tr_objective=" << c._tr_objective
      << ", _depth=" << c._depth
      << ", _unreach=" << c._num_unreach
      << endl;
  out << "Gates:" << endl;
  for (int i = 1; i <= c._num_gates; i++)
    out << "  " << c._gates[i] << endl;
  out << "Original constraints (" << c._num_constrs << "):" << endl;
  for (int i = 0; i < c._num_constrs; i++)
    out << "  " << c._constrs[i];
  out << endl;
  out  << "Constrained gates (" << c._constr_gates.size() << "): " << endl;
  for (int i = 0; i < (int)c._constr_gates.size(); i++)
    out << "  " << c._constr_gates[i];
  out << endl;
  out << "JFront:" << endl
      << "  " << c._jfront << endl;
  cout << "JFront size: " << c._jfront.size() << endl;
  cout << "Objective: " << c._objective << endl;
  return out;
}

// Writes out the circuit in GraphML format to the specified output stream
void circuit::as_graphml(ostream& out)
{
  // preamble
  out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl
      << "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns/graphml\""
      << " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
      << " xmlns:y=\"http://www.yworks.com/xml/graphml\""
      << " xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns/graphml"
      << " http://www.yworks.com/xml/schema/graphml/1.0/ygraphml.xsd\">" << endl
      << "  <key for=\"node\" id=\"d0\" yfiles.type=\"nodegraphics\"/>" << endl
      << "  <key attr.name=\"description\" attr.type=\"string\" for=\"node\" id=\"d1\"/>" << endl
      << "  <key for=\"edge\" id=\"d2\" yfiles.type=\"edgegraphics\"/>" << endl
      << "  <graph id=\"G\" edgedefault=\"directed\">" << endl;
  for (int i = 1; i <= _num_gates; i++) {
    gate& g = _gates[i];
    // render node
    out << "  <node id=\"" << g.id() << "\">" << endl
        << "    <data key=\"d0\">" << endl
        << "      <y:ShapeNode>" << endl
        << "        <y:NodeLabel visible=\"true\" textColor=\"#000000\">";
    switch (g.type()) {
    case gate::IN: /*out << "I";*/ break;
    case gate::OUT: /*out << "O";*/ break;
    case gate::AND: out << "&amp;/"; break;
    default: out << "???"; break;
    }
    out << g.tv();
    out << "</y:NodeLabel>" << endl;
    // shape depends on whether its IN/OUT or not
    if ((g.type() == gate::IN) || (g.type() == gate::OUT))
      out << "        <y:Shape type=\"rectangle\"/>" << endl;
    else
      out << "        <y:Shape type=\"ellipse\"/>" << endl;
    // color depends on the status:
    if (g.just()) {
      // gate is justfied (green)
      out << "        <y:Fill color=\"#76EE00\"/>" << endl;
    } else {
      // gate unjustified (red)
      out << "        <y:Fill color=\"#FF2400\"/>" << endl;
    }
    // line thickness depends on whether constained or not
    out << "        <y:BorderStyle color=\"#000000\" type=\"line\" width=\""
        << (g.constrained() ? "4.0" : "1.0") << "\"/>" << endl;
    out << "      </y:ShapeNode>" << endl
        << "    </data>" << endl
        << "    <data key=\"d1\">"
        << "id=" << g.id()
        << "</data>" << endl
        << "  </node>" << endl;
    // render edges to children
    if (g.left_id() != 0) {
      gate& c = _gates[abs(g.left_id())];
      bool neg = (g.left_id() < 0);
      // negated edges are dashed
      out << "  <edge source=\"" << c.id() << "\" target=\"" << g.id() << "\">" << endl
      << "    <data key=\"d2\">" << endl
            << "      <y:PolyLineEdge>" << endl
            << "        <y:Arrows source=\"none\" target=\"short\"/>" << endl
            << "        <y:LineStyle type=\"" << ((neg) ? "dashed" : "line") << "\"/>" << endl
            << "      </y:PolyLineEdge>" << endl
            << "    </data>" << endl
            << "  </edge>" << endl;
    }
    if (g.right_id() != 0) {
      gate& c = _gates[abs(g.right_id())];
      bool neg = (g.right_id() < 0);
      // negated edges are dashed
      out << "  <edge source=\"" << c.id() << "\" target=\"" << g.id() << "\">" << endl
      << "    <data key=\"d2\">" << endl
            << "      <y:PolyLineEdge>" << endl
            << "        <y:Arrows source=\"none\" target=\"short\"/>" << endl
            << "        <y:LineStyle type=\"" << ((neg) ? "dashed" : "line") << "\"/>" << endl
            << "      </y:PolyLineEdge>" << endl
            << "    </data>" << endl
            << "  </edge>" << endl;
    }
  }
  out << "  </graph>" << endl
    << "</graphml>" << endl
    << flush;
}

// Logic simulates the circuit for given input, ignoring the constraints;
// The size of ins should match the number of inputs, ins[i] contains a
// truth-value for the input i
void circuit::simulate(vector<tvs>& ins)
{
  assert((int)ins.size() == _num_ins);
  for (int i = _num_gates; i >= 1; i--) {
    gate& g = _gates[i];
    if (g.type() == gate::IN) {
      g.set_tv(ins[i - (_num_gates - _num_ins + 1)]);
    } else {
      g.set_tv(calculate_tv(g));
    }
  }
}

// Assigns and propagates the constraints through the circuit; the constrained gates
// are marked as such; returns false if a conflict has been detected during the
// propagation, true otherwise.
bool circuit::propagate_constrs(void)
{
  assert(_gate_queue.empty());
#if TRACE
  cout << "propagating the initial " << _num_constrs << " constraints." << endl;
#endif
  // assign and enqueue the initial constraints
  for (int i = 0; i < _num_constrs; i++) {
    gate& g = _gates[abs(_constrs[i])];
    tvs tv = (tvs)(_constrs[i] > 0);
    if (g.constrained() && g.tv() != tv) // conflict
      return false;
    g.set_tv(tv);
    g.set_constrained(true);
    _constr_gates.push_back(g.id());
    _gate_queue.push_back(g.id());
  }
  // work through the queue
  while (!_gate_queue.empty()) {
    gate& g = _gates[_gate_queue.front()];
    gate& g_left = _gates[abs(g.left_id())];
    gate& g_right = _gates[abs(g.right_id())];
    tvs tv = g.tv();
    tvs tv_l = tv_left(g);
    tvs tv_r = tv_right(g);
#if TRACE
    cout << "  picked " << g.id() << " from queue, type=" << g.type()
         << ", tv=" << tv << ", tv_l=" << tv_l << ", tv_r=" << tv_r << endl;
#endif
    // lookup the action
    bcp_actions act = lookup_bcp1_action(g.type(), tv_r, tv_l, tv);
#if TRACE
    cout << "  action=" << act << ", tv=" << tv << ", tv_l=" << tv_l
         << ", tv_r=" << tv_r << endl;
#endif
    if (act == CONFLICT)
      return false;
    else if (act == NOOP)
      ;
    else {
      if ((act & PF) && (!g.constrained())) {
        // propagate forward - set the new tv, make the gate constrained
        // and put its parents onto the queue
        g.set_tv(tv);
        g.set_constrained(true);
        for (int i = 0; i < g.num_parents(); i++)
          _gate_queue.push_back(abs(g.parent_ids()[i]));
        _constr_gates.push_back(g.id());
      }
      if ((act & PC0) && (!g_left.constrained())) {
        // propagate left child - set the tv of the child, make it constrained
        // and put it onto the queue; also, since its becoming constrained, put
        // all its parents, except g, onto the queue
        g_left.set_tv((g.left_id() > 0) ? tv_l : neg(tv_l));
        g_left.set_constrained(true);
        _gate_queue.push_back(g_left.id());
        for (int i = 0; i < g_left.num_parents(); i++) {
          int par_id = abs(g_left.parent_ids()[i]);
          if (par_id != g.id())
            _gate_queue.push_back(par_id);
        }
        _constr_gates.push_back(g_left.id());
      }
      if ((act & PC1) && (!g_right.constrained())) {
        // propagate right child - set the tv of the child, make it constrained
        // and put it onto the queue
        g_right.set_tv((g.right_id() > 0) ? tv_r : neg(tv_r));
        g_right.set_constrained(true);
        _gate_queue.push_back(g_right.id());
        for (int i = 0; i < g_right.num_parents(); i++) {
          int par_id = abs(g_right.parent_ids()[i]);
          if (par_id != g.id())
            _gate_queue.push_back(par_id);
        }
        _constr_gates.push_back(g_right.id());
      }
    }
    // done with this one
    _gate_queue.pop_front();
  }
  // no conflict; do additional calculations that depend on the constraints to
  // be propagated (TODO: probably should not be here ...)
  calculate_depths();
  return true;
}

// Makes a random assignment, while not touching the constrained gates;
// All non-constrained gates, except inputs, are given truth-values from
// their children (NOTE: not used, see prepare_for_try() instead)
void circuit::make_assignment(void)
{
  // go backwards
  for (int i = _num_gates; i >= 1; i--) {
    gate& g = _gates[i];
    if (!g.constrained()) {
      g.set_tv((g.type() == gate::IN) ? (tvs)random_toss() : calculate_tv(g));
    }
  }
}

// Calculates statuses (tv, just and cv) of the gates from scratch based on the
// truth-values of the inputs; returns true if the circuit is satisfied, i.e.
// there are no unjustified gates
bool circuit::calculate_status(void)
{
  // run from the inputs to outputs, set justification status, and
  // clash value
  _jfront.clear();
  _objective = 0;
  for (int i = _num_gates; i >= 1; i--) {
    gate& g = _gates[i];
    if ((g.type() != gate::IN) && (!g.constrained()))
      g.set_tv(calculate_tv(g));
    g.set_just(check_justified(g));
    g.set_objective(calculate_objective(g));
    if (!g.just()) {
      _jfront.insert(g.id());
      _objective += g.objective();
    }
  }
  return _jfront.empty();
}

// Prepares the circuit for a new try (makes a random assignment, re
// calculates status, clears caches, etc); if the pointer init is not NULL,
// the it is assumed to point to a vector of the size _num_gates+1 which
// contains a partial assignment to start with: -1 means "not set", o/w
// 0 or 1. Note that if the assignment is set for a constrained gate, its
// ignored.
void circuit::prepare_for_try(vector<int>* init)
{
  // run from the inputs to outputs, make assignment, set justification status,
  // and clash value; reset trial generations
  _jfront.clear();
  _objective = 0;
  _jcache.clear();
  _tr_gen = 0;
#if (STATS & 0x00000200)
  _unjust_nonconstr = 0;
#endif
  for (int i = _num_gates; i >= 1; i--) {
    gate& g = _gates[i];
    if (!g.constrained()) {
      // see if we have an initial truth-value for this gate
      int tv = (init != NULL) ? (*init)[i] : -1;
      // if yes, use it, otherwise either random, or from children
      if (tv >= 0)
        g.set_tv((tvs)tv);
      else
        g.set_tv((g.type() == gate::IN) ? (tvs)random_toss() : calculate_tv(g));
    }
    g.set_just(check_justified(g));
    g.set_objective(calculate_objective(g));
    if (!g.just()) {
      _jfront.insert(g.id());
      _objective += g.objective();
    }
    g.reset_tr_gens();
  }
}

// Similar to prepare_for_try(), except the caches etc are not cleared -- only
// the assignment to inputs is reset, and propagated forward; use this to make
// restarts during a try
void circuit::restart(void)
{
  // run from the inputs to outputs, make assignment, set justification
  // status, and clash value
  _jfront.clear();
  _objective = 0;
  for (int i = _num_gates; i >= 1; i--) {
    gate& g = _gates[i];
    if (!g.constrained())
      g.set_tv((g.type() == gate::IN) ? (tvs)random_toss() : calculate_tv(g));
    g.set_just(check_justified(g));
    g.set_objective(calculate_objective(g));
    if (!g.just()) {
      _jfront.insert(g.id());
      _objective += g.objective();
    }
  }
}

// Prepares the circuit for the try-run
void circuit::prepare_trial(void)
{
  _tr_gen++;
  _tr_objective = _objective;
}

// Flips the truth-value of gates()[id]; the state of the circuit is updated
// accordingly; the new truth-value is propagated thought the circuit in a
// manner controlled by param::pu, param::pd and param::ps parameters; if
// param::bc is true, returns false in case the flip leads to a local conflict;
bool circuit::flip(int id, bool trial)
{
  vector<int> ids(1, id);
  return multiflip(ids, trial);
}

// Flips the truth-values of gates the with specified id's; the state of the
// circuit is updated accordingly; the new truth-value is propagated thought
// the circuit in a manner controlled by the global param::prop_up,
// param::prop_down and param::prop_side parameters; returns false in case
// the flip leads to a conflict;
bool circuit::multiflip(vector<int>& ids, bool trial)
{
  return multiflip_forward(ids, trial);
}

// Flips the truth-values of gates the with specified id's - optimized for
// forward-only propagation; note that there're no conflicts in forward
// propagation, and since the state of a gate is fully determined by its
// children, using a pqueue allows to do the propagation and the state
// updates at the same time.
//
// Note that the algorithm here is very different from the generic propagation.
// To take advantage of the fact that the state of gate depends only on the
// states of its children, we must have all children already processed before
// setting the gate's truth-value. Hence, as opposed to the generic algorithm,
// each gate sets its own truth-value, rather than the truth-value of its
// parents. So, the BCP truth-value is set into the proper truth-value, and
// we reuse the bcp_tv field only to indicate whether or not the gate has to
// update its truth-value: if bcp_tv is defined, then yes; if UNDEF, then no.
bool circuit::multiflip_forward(vector<int>& ids, bool trial)
{
  long tr = trial ? _tr_gen : 0;
#if TRACE
  cout << ((trial) ? "try-" : "real-") << "flipping gates " << endl;
#endif
  assert(_gate_pqueue.empty());
  for (int i = 0; i < (int)ids.size(); i++) {
    gate& g = _gates[ids[i]];
    assert(!g.constrained());
    assert(defined(g.tv(tr)));
#if TRACE
    cout << "  " << g << endl;
#endif
    g.set_tv(neg(g.tv(tr)), tr);
    g.set_bcp_tv(TRUE); // TRUE will only be set for the initial gates ...
    _gate_pqueue.push_back(g.id());
    if (!trial)
      tracer::after_gate_state_change(g, neg(g.tv()), g.just(), g.objective());
  }
  while (!_gate_pqueue.empty()) {
    gate& g = _gates[_gate_pqueue.front()];
#if TRACE
    cout << "  picked " << g << endl;
    cout << "  tv=" << tv(g, trial)
         << ", tv_l=" << tv_left(g, trial)
         << ", tv_r=" << tv_right(g, trial)
         << ", bcp_tv=" << g.bcp_tv()
         << endl;
#endif
    // A gate can be on the queue for two reasons:
    //   1. its been scheduled during BCP by one of its children because the
    //      truth-value of the child has changed, and so the truth-value of
    //      the gate may need to be updated, and, so it its state.
    //   2. its been schedules by one of its children, because the state of the
    //      child has changed (but not tv), and so we may need to update its
    //      state
    // These two situations are distinguished by whether or not bcp_tv is
    // set to FALSE (tv may change) or UNDEF

    // remember the current state -- we will need it to make incremental
    tvs old_tv = g.tv(tr);
    bool old_just = g.just(tr);
    int old_objective = g.objective(tr);
    // tells whether we have already scheduled the parents ...
    bool parents_scheduled = false;
    if (defined(g.bcp_tv()) && !g.constrained()) {
      // if bcp_tv is TRUE, then this is an initial gate - schedule its
      // parents anyway; if bcp_tv is FALSE, take a look at the new tv for
      // the gate -- its different from current, accept it, and schedule the
      // parents
      if (g.bcp_tv() == TRUE) {
        parents_scheduled = true;
      } else {
        tvs new_tv = calculate_tv(g, trial);
        if (g.tv(tr) != new_tv) {
          g.set_tv(new_tv, tr);
          parents_scheduled = true;
        }
      }
      if (parents_scheduled) {
        for (int i = 0; i < g.num_parents(); i++) {
          gate& g_p = _gates[abs(g.parent_ids()[i])];
          // Note: if one of the flipped gates is in the fanout of other ...
          // ok, lets trust that it must be flipped
          if (g_p.bcp_tv() != TRUE)
            g_p.set_bcp_tv(FALSE);
          _gate_pqueue.push_back(g_p.id());
        }
      }
    }
    // make state updates ...
    g.set_just(check_justified(g, trial), tr);
    g.set_objective(calculate_objective(g, trial), tr);
    // see if anything changed
    bool just_changed = old_just != g.just(tr);
    bool objective_changed = old_objective != g.objective(tr);
    // update the jfront and the clash value for the circuit
    if (just_changed) {
      if (old_just) { // became unjustified
        if (!trial)
          _jfront.insert(g.id());
        (trial ? _tr_objective : _objective) += g.objective(tr);
      } else { // became justified
          if (!trial)
            _jfront.erase(g.id());
          (trial ? _tr_objective : _objective) -= old_objective;
      }
    } else if (!old_just) { // stayed unjustified - may need to update objective
        long delta = g.objective(tr) - old_objective;
        (trial ? _tr_objective : _objective) =
            (trial ? _tr_objective : _objective) + delta;
    }
    // finally, if anything has changed, and the parents are not scheduled
    // yet, schedule them
    bool state_changed = just_changed || objective_changed || parents_scheduled;
#if TRACE
    cout << "  updated state; state " << (state_changed ? "" : "didn't")
         << " change: " << g << endl;
#endif
    // if the state has changed, queue parents
    if (state_changed && !parents_scheduled) {
      for (int i = 0; i < g.num_parents(); i++) {
        gate& g_p = _gates[abs(g.parent_ids()[i])];
        _gate_pqueue.push_back(g_p.id());
      }
    }
    // done with this gate
    g.set_bcp_tv(UNDEF);
    _gate_pqueue.pop();
    if (parents_scheduled) // i.e. because of BCP
      stats::after_bcp_prop_dequeue();
    stats::after_bcp_state_dequeue();
    if (!trial && state_changed)
      tracer::after_gate_state_change(g, old_tv, old_just, old_objective);
  }
  // done
  return true;
}


// Calculates and returns the justifications for a gate abs(ga) under the
// truth-value assignment (sign of ga). Note that this function does not take
// into account the constrained gates
justifications& circuit::calculate_justifications(int ga)
{
  gate& g = _gates[abs(ga)];
#if TRACE
  cout << "calculating justifications for " << ga << " : " << g << endl;
#endif
  // check the cache first
  if (_jcache.count(ga)) {
#if TRACE
    cout << "  found in the cache: " << _jcache[ga] << endl;
#endif
    return _jcache[ga];
  }

  // not in the cache, calculate from scratch, and put it into cache
  justifications justs;
  gate& g_left = _gates[abs(g.left_id())];
  gate& g_right = _gates[abs(g.right_id())];
  switch(g.type()) {
  case gate::IN:
    // justification for IN gate is the gate itself
    {
      justification j;
      j.insert(ga);
      justs.insert(justs.end(), j);
    }
    break;
  case gate::OUT:
    // justification for OUT gate is its argument
#if (STATS & 0x00000010)
    if (true) { // recurse always
#else
    if (g_left.num_parents() <= 1) {
#endif
      justs = calculate_justifications((ga > 0) ? g.left_id() : -g.left_id());
    } else {
      justification j;
      j.insert((ga > 0) ? g.left_id() : -g.left_id());
      justs.insert(justs.end(), j);
    }
    break;
  case gate::AND:
    // justifications for a negative AND gate is a union of negative
    // justifications of its left and its right children - note that there's
    // no possibility for explosion here, because the size of the new
    // justification is simply a sum of the sizes of the original's
    if (ga < 0) {
      // if left is a positive single-parent, then recurse, otherwise just add
      // it itself
#if (STATS & 0x00000010)
      if (true) { // recurse always
#else
      if ((g.left_id() > 0) && (g_left.num_parents() <= 1)) {
#endif
        justs = calculate_justifications(-g.left_id());
      } else {
        justification j;
        j.insert(-g.left_id());
        justs.insert(justs.end(), j);
      }
      // same for the right
#if (STATS & 0x00000010)
      if (true) { // recurse always
#else
      if ((g.right_id() > 0) && (g_right.num_parents() <= 1)) {
#endif
        justifications& right_justs = calculate_justifications(-g.right_id());
        justs.insert(justs.end(), right_justs.begin(), right_justs.end());
      } else {
        justification j;
        j.insert(j.end(), -g.right_id());
        justs.insert(justs.end(), j);
      }
    }
    // justifications for a positive AND gate pose a problem: it is a
    // conjunction of positive justifications of its left and right children;
    // in terms of sets, we need to take a cross-union of the children's
    // justifications, i.e. a set that consists of a union of each pair of
    // of left and right children's justifications - thus the size of new
    // justification is a product of sizes of the children's justifications,
    // which may get really large (consider the fact that this is a recursive
    // call as well !). The only time the size of the product is under control,
    // is when both children give a singleton (TODO: actually, could I handle
    // the case when one of them only, and another one doesn't ?). Note that
    // if one of the justifications is empty, the resulting justifications is
    // also empty
    else {
      justification* left_just_ptr = NULL;
      bool delete_left = false;
#if (STATS & 0x00000010)
      if (true) { // recurse always
#else
      if ((g.left_id() > 0) && (g_left.num_parents() <= 1)) {
#endif
        // try to recurse, if the size is too much, give up
        justifications& left_justs = calculate_justifications(g.left_id());
        if (left_justs.size() == 1)
          left_just_ptr = &left_justs[0]; // hack (relies on vector)
      }
      if (left_just_ptr == NULL) {
        // create justification with the left_id only
        left_just_ptr = new justification();
        left_just_ptr->insert(g.left_id());
        delete_left = true;
      }
      justification* right_just_ptr = NULL;
      bool delete_right = false;
#if (STATS & 0x00000010)
      if (true) { // recurse always
#else
      if ((g.right_id() > 0) && (g_right.num_parents() <= 1)) {
#endif
        // try to recurse, if the size is too much, give up
        justifications& right_justs = calculate_justifications(g.right_id());
        if (right_justs.size() == 1)
          right_just_ptr = &right_justs[0]; // hack (relies on vector)
      }
      if (right_just_ptr == NULL) {
        // create justification with the right_id only
        right_just_ptr = new justification();
        right_just_ptr->insert(g.right_id());
        delete_right = true;
      }
      // put the union of left and right into justs
      justification j;
      justification_ins_iter j_iter(j, j.begin());
      set_union(left_just_ptr->begin(), left_just_ptr->end(),
          right_just_ptr->begin(), right_just_ptr->end(), j_iter);
      justs.insert(justs.end(), j);
      if (delete_left)
        delete left_just_ptr;
      if (delete_right)
        delete right_just_ptr;
    }
    break;
  }
  // ok, all good, put it into the cache, and return the reference
  _jcache[ga] = justs;
#if TRACE
  cout << "  returning " << justs << endl;
#endif
  return _jcache[ga];
}

// Calculates the depths of the gates in the circuit
void circuit::calculate_depths(void)
{
  _depth = 0; // this is the circuit's depth
  _num_unreach = 0; // the number of unreachable gates
  for (int i = 1; i <= _num_gates; i++) {
    gate& g = _gates[i];
    if (g.constrained()) {
      g.set_depth(0);
    }
    else if (g.depth() == 0)
      _num_unreach++;
    if (g.depth() > _depth)
      _depth = g.depth();
    int new_depth = g.depth() + 1;
    gate& g_left = _gates[abs(g.left_id())];
    if (new_depth > g_left.depth())
      g_left.set_depth(new_depth);
    gate& g_right = _gates[abs(g.right_id())];
    if (new_depth > g_right.depth())
      g_right.set_depth(new_depth);
  }
  // done
  if (_num_unreach) {
    cout << "Warning: the circuit contains unreachable gates."
         << endl;
  }
}

// Calculates and returns the (try) truth-value of the gate under
// current assignment (the gate should not be IN, b/c there's nothing to
// calculate for IN gates)
tvs circuit::calculate_tv(gate& g, bool trial) const
{
  switch (g.type()) {
  case gate::IN:
    assert(false);
    break;
  case gate::OUT:
    return tv_left(g, trial);
  case gate::AND:
    return (tvs)(tv_left(g, trial) & tv_right(g, trial));
  }
  return UNDEF;
}

// Checks and returns the (try) justification status of gate g
bool circuit::check_justified(gate& g, bool trial) const
{
  // inputs are always justified
  if (g.type() == gate::IN)
    return true;
  return calculate_tv(g, trial) == g.tv(trial ? _tr_gen : 0);
}

// Calculates and returns the (try) objective value of gate g; the calculation
// of objective value for some gates depends on whether or not the gate is
// justified (thus, the method works on the assumption that the (try) just
// status, and the (try) truth-values are already set appropriately)
long circuit::calculate_objective(gate& g, bool trial) const
{
  long tr = trial ? _tr_gen : 0;
  // standard objective - num unjust gates
  return (g.just(tr) ? 0 : 1);
}

// Assigns given truth-value to the gate's bcp_tv; returns 1 if the gate did
// not have bcp_tv yet; 0 if it did and its value agrees with tv, or if its
// constrained and its truth-value agrees with tv; -1 if it has an opposite
// bcp_tv, or its constrained to an opposite truth-value.
int circuit::bcp_assign_gate(gate&g, tvs tv)
{
  if (g.constrained()) {
    if (tv != g.tv()) {
      // conflict against the constrained truth-value; note that constrained
      // gates are never assigned truth-values, and so their try-values are
      // irrelevant
      return -1;
    }
    // all set
    return 0;
  }
  if (g.bcp_tv() != UNDEF) {
    if (tv != g.bcp_tv()) {
      // conflict against the previous bcp_tv
      return -1;
    }
    // all set
    return 0;
  }
  // not constrained, and doesn't have bcp_tv(), make the assignment
  g.set_bcp_tv(tv);
  return 1;
}

// Checks and returns the conflict type during BCP. The 'tv' is the truth-value
// to be checked.
circuit::bcp_conflict_type circuit::bcp_check_conflict(gate&g, tvs tv)
{
  if (g.constrained()) {
    // constrained gate : check if the truth-value agrees or not
    return (tv == g.tv()) ? NO_CONFLICT_AGREE : HARD_CONFLICT;
  } else if (g.bcp_tv() == UNDEF) {
    // no BCP truth-value yet
    return NO_CONFLICT_UNASS;
  } else {
    // has BCP truth-value : either soft conflict, or agrees
    return (tv == g.bcp_tv()) ? NO_CONFLICT_AGREE : SOFT_CONFLICT;
  }
}

//
// -------------------------  Local implementations -------------------------
//
namespace {
}
