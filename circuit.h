/*
 Copyright (c) 2009-2011, Anton Belov, anton.belov@ucd.ie
 */

// ----------------------------------------------------------------------------
//
// circuit.h - declarations and definitions of (possibly constrained) circuits
//             and other related data structures
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//   1. All gate methods that accept "tr" parameter allow to work with 
//   try-value of gates. When tr > 0, the get_ methods will return the try 
//   values if the try generation of the gate is the same as tr, and regular 
//   values otherwise; the set_ methods will set the try values and the try 
//   generation if tr > 0. In short, tr == 0 means "work with real values", 
//   tr > 0 means "work with try-values".
//

#ifndef _CIRCUIT_H_
#define _CIRCUIT_H_

#include <cassert>
#include <climits>
#include <iostream>
#include <map>
#include <set>
#include <stdexcept>
#include "aiger.h"
#include "utils.h"

//
// ----------------------- Data structures and types --------------------------
//


// -- Truth-values --

// the bit patterns are made to optimize negation
enum tvs { FALSE = 0x0, TRUE = 0x1, UNDEF = 0x2, UNDEF2 = 0x3 };

// output operator for truth-values
inline std::ostream& operator<<(std::ostream& out, const tvs& tv) {
  switch (tv) {
  case FALSE: out << "0"; break;
  case TRUE: out << "1"; break;
  case UNDEF: case UNDEF2: out << "?"; break;
  default: out << "???"; break;
  }
  return out;
}

// returns true if the truth-value defined (i.e. TRUE or FALSE)
inline bool defined(tvs tv) { return !(tv & 0x02); }
// returns the negation of the specified truth-value (note that UNDEF0 becomes
// UNDEF1 and vice versa)
inline tvs neg(tvs tv) { return (tvs)(tv ^ 0x01); }


// -- BCP actions --

enum bcp_actions {
  CONFLICT = -1,    // conflict
  NOOP = 0,         // noop
  PF = 1,           // propagate forward
  PO = 2,           // propagate other (i.e. to sibling)
  PC0 = 4,          // propagate child_0
  PC1 = 8           // propagate child_1
};

// output operator for BCP actions
inline std::ostream& operator<<(std::ostream& out, const bcp_actions& ba)
{
  if (ba == CONFLICT)
    out << "CONFLICT ";
  else if (ba == NOOP)
    out << "NOOP ";
  else {
    if (ba & PF)
      out << "PF ";
    if (ba & PO)
      out << "PO ";
    if (ba & PC0)
      out << "PC0 ";
    if (ba & PC1)
      out << "PC1 ";
  }
  return out;
}

// -- Gate --

// This the gate; gates are designed to be mostly immutable - the type, 
// children and the id cannot be changed once the gate has been created.
class gate
{
  public:

    // gate types constants
    enum types { IN = 1, OUT = 2, AND = 3 };

    // lifecycle

    // constructor
    gate(int id, types type, int left_id, int right_id, int num_parents);

    // accessors/mutators

    // -- gate's state (dynamic)

    // returns current (try) truth-value
    tvs tv(long tr = 0) const;
    // sets (try) truth-value
    void set_tv(tvs tv, long tr = 0);
    // returns true if the gate is (try) justified
    bool just(long tr = 0) const;
    // sets the (try) justification status
    void set_just(bool just, long tr = 0);
    // returns the (try) objective value of this gate
    long objective(long tr =  0) const;
    // sets the (try) objective value of this gate
    void set_objective(long objective, long tr = 0);
    // returns the current BCP truth-value
    tvs bcp_tv(void) const;
    // sets the BCP truth-value
    void set_bcp_tv(tvs bcp_tv);
    // resets all the trial generations
    void reset_tr_gens(void);

    // -- gate's structure (almost static)

    // returns gate id (> 0)
    int id(void) const;
    // returns gate type
    types type(void) const;
    // returns true when the gate is constrained
    bool constrained(void) const;
    // set the constrained status
    void set_constrained(bool constrained);
    // id of the left child (<0 if negated, 0 if none)
    int left_id(void) const;
    // id of the right child (<0 if negated, 0 if none)
    int right_id(void) const;
    // returns the number of parents
    int num_parents(void) const;
    // returns the array of parent ids (undefined if 0 parents); each id may be
    // negated
    const int* parent_ids(void) const;
    // returns the depth of the gate (max distance from constrained gate)
    int depth(void) const;
    // sets the depth
    void set_depth(int depth);

    // functionality

    // returns true when this gate is structurally the same g;
    bool operator==(const gate& g) const;
    // returns true when the (try) state of this gate is the same as g
    bool same_state(const gate& g, long tr = 0) const;

//friend:

    // output operator for gates
    friend std::ostream& operator<<(std::ostream& out, const gate& g);

  private:
    // gate's state (dynamic)
    tvs _tv;                // current tv
    bool _just;             // justified or not
    long _objective;        // objective value
    tvs _bcp_tv;            // truth-value for BCP
    // the try-versions of gate's state and their generations
    tvs _tr_tv;             // try tv
    long _tr_tv_gen;        // try tv generation
    bool _tr_just;          // try justified
    long _tr_just_gen;      // try justified generation
    long _tr_objective;     // try objective value
    long _tr_objective_gen; // try objective value generation
    // gate's structure (static)
    int _id;                // gate id
    types _type;            // type
    bool _constrained;      // true when the gate is constrained
    int  _depth;            // distance to the farthest constrained gate
    int _left_id;           // left child id
    int _right_id;          // right child id
    int _num_parents;       // the number of parents
};

// constructor
inline gate::gate(
    int id, gate::types type, int left_id, int right_id, int num_parents) :
    _tv(UNDEF), _just(false), _bcp_tv(UNDEF),
    _tr_tv(UNDEF), _tr_tv_gen(-1),
    _tr_just(false), _tr_just_gen(-1),
    _tr_objective(0), _tr_objective_gen(-1),
    _id(id), _type(type), _constrained(false),
    _depth(0),
    _left_id(left_id), _right_id(right_id), _num_parents(num_parents)
{
}
// returns current (try) truth-value
inline tvs gate::tv(long tr) const {
  return (tr == _tr_tv_gen) ? _tr_tv : _tv;
}
// sets (try) truth-value
inline void gate::set_tv(tvs tv, long tr) {
  (tr ? (_tr_tv_gen = tr, _tr_tv) : _tv) = tv;
}
// returns true if the gate is (try) justified
inline bool gate::just(long tr) const {
  return (tr == _tr_just_gen) ? _tr_just : _just;
}
// sets the (try) justification status
inline void gate::set_just(bool just, long tr) {
  (tr ? (_tr_just_gen = tr, _tr_just) : _just) = just;
}
// returns the (try) objective value of this gate
inline long gate::objective(long tr) const {
  return (tr == _tr_objective_gen) ? _tr_objective : _objective;
}
// sets the (try) objective value for this gate
inline void gate::set_objective(long objective, long tr) {
  (tr ? (_tr_objective_gen = tr, _tr_objective) : _objective) = objective;
}
// returns the current BCP truth-value
inline tvs gate::bcp_tv(void) const {
  return _bcp_tv;
}
// sets the BCP truth-value
inline void gate::set_bcp_tv(tvs bcp_tv) {
  _bcp_tv = bcp_tv;
}
// resets all the trial generations
inline void gate::reset_tr_gens(void) {
  _tr_tv_gen = _tr_just_gen = _tr_objective_gen = -1;
}
// returns gate id (> 0)
inline int gate::id(void) const {
  return _id;
}
// returns gate type
inline gate::types gate::type(void) const {
  return _type;
}
// returns true when the gate is constrained
inline bool gate::constrained(void) const {
  return _constrained;
}
// sets the constrained status
inline void gate::set_constrained(bool constrained) {
  _constrained = constrained;
  _tr_tv = _tv; // fixed
}
// id of the left child (<0 if negated, 0 if none)
inline int gate::left_id(void) const {
  return _left_id;
}
// id of the right child (<0 if negated, 0 if none)
inline int gate::right_id(void) const {
  return _right_id;
}
// returns the number of parents
inline int gate::num_parents(void) const {
  return _num_parents;
}
// returns the array of parent ids (undefined if 0 parents); each id may be
// negated
inline const int* gate::parent_ids(void) const {
  return (const int*)(this + 1);
}
// returns the depth
inline int gate::depth(void) const {
  return _depth;
}
// sets the depth
inline void gate::set_depth(int depth) {
  _depth = depth;
}

// output operator for gate types
inline std::ostream& operator<<(std::ostream& out, const gate::types& gt) {
  switch(gt) {
  case gate::IN: out << "IN"; break;
  case gate::OUT: out << "OUT"; break;
  case gate::AND: out << "AND"; break;
  default: out << "???"; break;
  }
  return out;
}

// -- Justifications --

// 'justification' of an assigned gate is its implicant; 'justifications' is a
// list (vector) of implicants; although there are no restrictions, in
// principle it would be desirable to keep the lists short - hence in the
// future I should consider either trying to get prime implicants (or almost
// prime), or returning only some of the implicants.

// the justification (implicant)
typedef std::set<int> justification;
typedef std::set<int>::iterator justification_iter;
typedef std::insert_iterator< std::set<int> > justification_ins_iter;
inline std::ostream& operator<<(std::ostream& out, justification& just) {
  out << "{ ";
  for (justification_iter iter = just.begin(); iter != just.end(); ++iter)
    out << *iter << " ";
  out << "}";
  return out;
}

// the justifications (list of implicants)
typedef std::vector<justification> justifications;
typedef std::vector<justification>::iterator justifications_iter;
inline std::ostream& operator<<(std::ostream& out, justifications& justs) {
  out << "{ ";
  for (int i = 0; i < (int)justs.size(); i++) {
    justification j = justs[i];
    out << j << " ";
  }
  out << "}";
  return out;
}


// -- Comparator for jfront --

// forward declare circuit
class circuit;

//
// This is a comparator for the gates in the _jfront when the jfront is ordered
//
class jfront_cmp {
public:
  // set the owning circuit
  void set_circuit(circuit* c) {
    _c = c;
  }
  // operator()(int left, int right) returns true when g[left] < g[right],
  // false otherwise; modify this operator to change the ordering in jfront,
  // remember that jfront is ordered such that the *largest* element according
  // to this order is at the top.
  // Note: implementation is after the definition of circuit
  bool operator()(int left, int right);
private:
  // the owning circuit
  static circuit* _c;
};


// -- Circuit --

//
// This is the constrained circuit. Conceptually, a circuit is a just bunch of
// gates, with some input and some output gates. Internally the circuit is laid
// out in one continuous block of memory such that the for every gate its
// children are on the right of the gate itself. The id of the gate is the same
// as the index in the array of gates in the circuit. This circuit is designed
// to be static, i.e. once created and laid out no gates are added/removed from
// the circuit.
//
class circuit
{
  public:

    // this inner class encapsulates internal access to gates, so we can change
    // the internal layout later on, if desired
    class gate_array {
      friend class circuit;
      public:
        // use this to access gates, even from circuit
        gate& operator[](int i) const;
      //private:
        gate** _storage;
    };

    // lifecycle

    // creates a new circuit from description in the specified file
    static circuit* create(std::string& in_file);
    // frees up the allocated resources
    static void destroy(circuit* c);
    // makes and returns a deep copy of a circuit
    static circuit* copy(circuit& from);
    // returns true when this circuit is structurally the same c
    bool operator==(const circuit& c) const;
    // returns true when the state of this circuit is the same as c
    bool same_state(const circuit& c) const;

    // debugging

    // writes out the circuit in GraphML
    void as_graphml(std::ostream& out);
    // logic simulates the circuit for given input, ignoring the constraints
    void simulate(std::vector<tvs>& ins);

    // accessors/mutators

    // returns the number of gates
    int num_gates(void) const;
    // returns the array of gates  [1,num_gates], topological order
    const gate_array& gates(void) const;
    // returns the number of inputs
    int num_ins(void) const;
    // returns the array of inputs [0,num_ins)
    const gate_array& ins(void) const;
    // returns the number of outputs
    int num_outs(void) const;
    // returns the array of outputs [0,num_outs)
    const gate_array& outs(void) const;
    // returns the number of constrains (original or all)
    int num_constrs(void) const;
    // returns the constraints (original or all)
    const int* constrs(void) const;
    // returns the jfront (all unjustified gates); note that there is no
    // explicit try jfront for performance reasons
    const int_pqueue<jfront_cmp>& jfront(void) const;
    // returns the (try) objective value of the gates in (virtual try) jfront
    long objective(bool trial = false) const;
    // returns the try-generation of this circuit
    long tr_gen(void) const;
    // returns the depth of the circuit
    int depth(void) const;
    // returns the number of unreachable gates in the circuit
    int num_unreach(void) const;

    // functionality
    
    // propagates constraints through the circuit
    bool propagate_constrs(void);
    // makes a random assignment, while respecting the constraints;
    void make_assignment(void);
    // calculates statuses of all gates from scratch bacse on the inputs;
    // returns true if the circuit is satisfied (i.e. no unjustified gates)
    bool calculate_status(void);
    // prepares the circuit for a new try (makes a random assignment, re
    // calculates status, clears caches, etc); if init is not NULL it contains
    // an initial (possibly partial) assignment
    void prepare_for_try(std::vector<int>* init = NULL);
    // similar to prepare_for_try(), except only the assignment to inputs is
    // reset, and propagated forward; use this to make restarts during a try
    void restart(void);
    // prepares the circuit for a try run - call this before flipping with
    // trial = true
    void prepare_trial(void);
    // flips the (try) truth-value of gates()[id]; the state of the circuit is
    // updated accordingly; the new (try) truth-value is propagated throught
    // the circuit, depending on the setting of param::pu, param::pd and
    // param::ps; if param::bc is true, returns false if a conflict is detected
    // and no changes are made to the circuit
    bool flip(int id, bool trial = false);
    // flips the truth-values of gates the with specified id's; the state of the
    // circuit is updated accordingly; the new truth-value is propagated thought
    // the circuit in a manner controlled by param::pu, param::pd and param::ps
    // parameters; if param::bc is true, returns false in case the flip leads to a
    // local conflict;
    bool multiflip(std::vector<int>& ids, bool trial = false);
    // this is a fast-track version for the case when only the forward
    // propagation is enabled
    bool multiflip_forward(std::vector<int>& ids, bool trial = false);
    // calculates and returns the justifications for a gate assignment (i.e. a
    // gate id plus truth-value given as a sign)
    justifications& calculate_justifications(int ga);

//friend:
    friend std::ostream& operator<<(std::ostream& out, const circuit& c);

  private:
    void* _mem;               // pointer to the memory allocated for the circuit
    int _num_gates;           // number of gates (incl. input and output)
    gate_array _gates;        // array of gates (in topological order)
    int _num_ins;             // number of inputs
    gate_array _ins;          // array of inputs
    int _num_outs;            // number of outputs
    gate_array _outs;         // array of outputs
    int _num_constrs;         // number of original constraints
    int* _constrs;            // original constraints (+/- gate id)
    int_pqueue< jfront_cmp > _jfront; // the jfront
    long _objective;          // the objective value of the gates in the jfront
    long _tr_gen;             // try generation
    long _tr_objective;       // try objective-value of the gates in jfront
    int _depth;               // depth of the circuit
    int _num_unreach;         // number of unreachable gates
    int_queue _gate_queue;    // queue used for various propagation tasks
    int_queue _gate_queue2;   // another queue
    int_pqueue<> _gate_pqueue;// priority queue (top=max) for flipping etc
    int_pqueue< std::greater<int> > _gate_pqueue2;// priority queue (top=min)
    std::vector<int> _constr_gates; // the indexes of all constrained gates
                              // (original and propagated)
    std::map<int, justifications> _jcache; // the cache for justifications

    // creates a new circuit from description in AIGER binary format
    static circuit* create_from_aig(void* in, aiger_get getter);
    // calculates the depths the gates, and as a side effect, the number of
    // unreachable gates; call after initial BCP
    void calculate_depths(void);
    // returns the (try) truth-value of gate g
    tvs tv(const gate& g, bool trial = false) const;
    // returns the (try) truth-value of left child of gate g (as seen by g)
    tvs tv_left(gate& g, bool trial = false) const;
    // returns the (try) truth-value of right child of gate g (as seen by g)
    tvs tv_right(gate& g, bool trial = false) const;
    // calculates the (try) truth-value of gate g
    tvs calculate_tv(gate& g, bool trial = false) const;
    // checks and returns the (try) justification status of g
    bool check_justified(gate& g, bool trial = false) const;
    // calculates the (try) objective value of gate g
    long calculate_objective(gate& g, bool trial = false) const;
    // assigns truth-value to bcp_tv of the gate, while checking for conflicts
    int bcp_assign_gate(gate& g, tvs tv);
    // conflict types for BCP: see circuit::bcp_check_conflict()
    enum bcp_conflict_type {
      NO_CONFLICT_UNASS = 0,  // no conflict: gate does not have BCP value
      NO_CONFLICT_AGREE,      // no conflict: BCP value agrees with previous
                              // or constrained truth-value
      SOFT_CONFLICT,          // conflict: BCP values disagree
      HARD_CONFLICT           // conflict: gate is constrained to opposite
    };
    friend std::ostream& operator<<(std::ostream& out,
                                    const bcp_conflict_type& c);
    // checks and returns the conflict type during the bcp
    bcp_conflict_type bcp_check_conflict(gate&g, tvs tv);
};

// gate_array access - returns the i-th gate
inline gate& circuit::gate_array::operator[](int i) const {
  return *_storage[i];
}
// returns the number of gates
inline int circuit::num_gates(void) const {
  return _num_gates;
}
// returns the array of gates [1,num_gates], topological order
inline const circuit::gate_array& circuit::gates(void) const {
  return _gates;
}
// returns the number of inputs
inline int circuit::num_ins(void) const {
  return _num_ins;
}
// returns the array of inputs
inline const circuit::gate_array& circuit::ins(void) const {
  return _ins;
};
// returns the number of outputs
inline int circuit::num_outs(void) const {
  return _num_outs;
};
// returns the array of outputs
inline const circuit::gate_array& circuit::outs(void) const {
  return _outs;
}
// returns the number of constrains
inline int circuit::num_constrs(void) const {
  return (_constr_gates.size() == 0) ? _num_constrs : _constr_gates.size();
}
// returns the ids of constrained gates
inline const int* circuit::constrs(void) const {
  return (_constr_gates.size() == 0) ? _constrs : &_constr_gates[0];
}
// returns the jfront (unjustified gates)
inline const int_pqueue<jfront_cmp>& circuit::jfront(void) const {
  return _jfront;
}
// returns the (try) objective value of the gates in (virtual try) jfront
inline long circuit::objective(bool trial) const {
  return trial ? _tr_objective: _objective;
}
// returns the try-generation of this circuit
inline long circuit::tr_gen(void) const {
  return _tr_gen;
}
// returns the depth of the circuit
inline int circuit::depth(void) const {
  return _depth;
}
// returns the number of unreachable gates in the circuit
inline int circuit::num_unreach(void) const {
  return _num_unreach;
}
// returns the (try) truth-value of the gate g (this is a convenience method
// that uses bool trial instead of the generation
inline tvs circuit::tv(const gate& g, bool trial) const {
  return g.tv(trial ? _tr_gen : 0);
}
// returns the (try) truth-value of left child of gate g (as seen by g)
inline tvs circuit::tv_left(gate& g, bool trial) const {
  tvs tv_left = _gates[abs(g.left_id())].tv(trial ? _tr_gen : 0);
  return (g.left_id() < 0 && defined(tv_left)) ? neg(tv_left) : tv_left;
}
// returns the truth-value of right child of gate g (as seen by g)
inline tvs circuit::tv_right(gate& g, bool trial) const {
  tvs tv_right = _gates[abs(g.right_id())].tv(trial ? _tr_gen : 0);
  return (g.right_id() < 0 && defined(tv_right)) ? neg(tv_right) : tv_right;
}
// output operator for bcp_conflict_type
inline std::ostream& operator<<(std::ostream& out,
                                const circuit::bcp_conflict_type& c)
{
  switch(c) {
  case circuit::NO_CONFLICT_UNASS: out << "NO_CONFLICT_UNASS"; break;
  case circuit::NO_CONFLICT_AGREE: out << "NO_CONFLICT_AGREE"; break;
  case circuit::SOFT_CONFLICT: out << "SOFT_CONFLICT"; break;
  case circuit::HARD_CONFLICT: out << "HARD_CONFLICT"; break;
  }
  return out;
}


// ------------------------  Global variable declarations ---------------------
//

//
// ------------------------ Global inline functions ---------------------------
//

// the implementation of the comparison for jfront ordering
// operator()(int left, int right) returns true when g[left] < g[right],
// false otherwise; modify this operator to change the ordering in jfront,
// remember that jfront is ordered such that the *largest* element according
// to this order is at the top.
inline bool jfront_cmp::operator()(int left, int right) {
  return _c->gates()[left].depth() < _c->gates()[right].depth();
}

//
// ------------------------ Global outline functions --------------------------
//


// Given a gate type, and the truth-values of inputs and outputs (unused inputs
// are ignored), returns the appropriate BCP action - the actions are taken 
// from the table for propagation in unassigned circuits; as a side effect, the
// values of inputs/outputs are set to the appropriate next state.
bcp_actions lookup_bcp1_action(gate::types t, tvs& in1, tvs& in0, tvs& out);
// Given a gate type, and the truth-values of inputs and outputs (unused inputs
// are ignored), returns the appropriate BCP action - the actions are taken 
// from the table for propagation to children in assigned circuits (ptable2); 
// as a side effect, the values of inputs/outputs are set to the appropriate 
// next state.
bcp_actions lookup_bcp2_action(gate::types t, tvs& in1, tvs& in0, tvs& out);
// Given a gate type, and the truth-values of this, the other and the parent
// nodes (unused are ignored), returns the appropriate BCP action - the actions 
// are taken from the table for propagation to parents and siblings in assigned 
// circuits (ptable3); as a side effect, the values are set to the appropriate 
// next state.
bcp_actions lookup_bcp3_action(gate::types t, tvs& oth, tvs& th, tvs& par);
// Returns alternatives to actions returned by lookup_bcp3_action; semantics
// as the same as lookup_bcp3_action, except NOOP means "no alternative".
bcp_actions lookup_bcp3a_action(gate::types t, tvs& oth, tvs& th, tvs& par);

#endif /* _CIRCUIT_H_ */
