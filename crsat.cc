/*
 Copyright (c) 2009-2011, Anton Belov, anton.belov@ucd.ie
 */

// ----------------------------------------------------------------------------
//
// crsat.cc - circuit SAT solver entry point
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <signal.h>
#include <unistd.h>
#include <vector>
#include "circuit.h"
#include "param.h"
#include "stats.h"
#include "tracer.h"
#include "utils.h"

using namespace std;

//
// -----------------------------  Global data   ------------------------------
//

// header text to print (update version info)
const char* HEADER =
    "# crsat v2.0/DEPTH (selection = max depth, objective = |unjust|)";

// when timeout is used, this variable is set to true by SIGALRM handler
bool timeout = false;

//
// ---------------------------  Local declarations ----------------------------
//

// a convenience method to check if the circuit is satisfied
bool satisfied(circuit& c);
// selects the gates for flipping using GWSAT heuristic
void select_gates_gwsat(circuit& c, vector<int>& selected, bool& random_walk);
// SIGALRM handler for timeouts
void sigalrm_handler(int sig);

//
// ------------------------  Exported implementations -------------------------
//

//
// Main entry point of the solver ...
//
int main(int argc, char *argv[], char *envp[])
{
  int sat = 20; // UNKNOWN

  cout << HEADER << endl;

  try {

    // read in parameters
    param::init(argc, argv);
    stats::after_params();

    // set up SIGALRM handler, if needed
    if (param::timeout != 0)
      signal(SIGALRM, sigalrm_handler);

    // create the circuit
    stats::before_input();
    circuit& c = *(circuit::create(param::input_file));
    stats::after_input(c);

    // do the initial constraints
    if (!c.propagate_constrs()) {
      cout << "detected conflict during preprocessing" << endl;
      return 0; // UNSAT
    }
    stats::after_preprocessing(c);

    // only info
    if (param::dump_stats) {
      stats::write_circuit_stats(c);
      exit(0);
    }
    // search ...
    init_random(param::rn_seed, param::rn_state);
    vector<int> selected;
    selected.reserve(c.num_gates() - c.num_constrs());
    stats::before_main_loop(c);
    tracer::before_main_loop(c);
    for (int tries = 0; tries < param::tries; tries++) {
      c.prepare_for_try();
      if (param::dump_gml) { // write and bail out ...
        char fname[1024];
        sprintf(fname, "%s.graphml", param::input_file.c_str());
        ofstream fout(fname);
        c.as_graphml(fout);
        fout.close();
        exit(0);
      }
      if (param::timeout) { // schedule SIGALRM
        alarm(param::timeout);
        timeout = false;
      }
      stats::before_try(c, tries);
      tracer::before_try(c, tries);
      int steps = 0;
      for (; (steps < param::cutoff) && !satisfied(c) && !timeout; steps++) {
        bool random_walk = false;
#if TRACE
        cout << "step " << steps << " started, objective=" << c.objective()
             << endl;
#endif
        tracer::before_selection(c, steps);
        select_gates_gwsat(c, selected, random_walk);
#if TRACE
        cout << "selected gates: ";
        copy(selected.begin(), selected.end(), ostream_iterator<int, char>(cout, " "));
        cout << endl;
        cout << "random_walk=" << random_walk << endl;
#endif
        stats::before_step(c, steps, selected.size());
        tracer::before_step(c, steps, selected);
#if TRACE
        cout << "flipping selected gates" << endl;
#endif
        c.multiflip(selected); // note that it may still give conflict
#if TRACE
        cout << "done" << endl;
#endif
        stats::after_step(c, steps, random_walk);
        tracer::after_step(c, steps);
      }
      // make sure the circuit is in proper state, and validate the result if
      // SAT
      if (satisfied(c)) {
        // validate
        assert(c.calculate_status());
        sat = 10;
        if (param::print_sat_assign) {
          cout << "# satisfying assignment" << endl << "v ";
          for (int i = c.num_gates()-c.num_ins()+1; i <= c.num_gates(); i++)
            cout << (c.gates()[i].tv() ? "" : "-") <<  i << " ";
          cout << "0" << endl;
        }
      }
      stats::after_try(c, tries, steps);
      tracer::after_try(c, tries, steps);
    }
    stats::after_main_loop(c);
    tracer::after_main_loop(c);
    // done, clean up
    circuit::destroy(&c);
  } catch (param::parse_error& e) {
    cout << "Invalid command line arguments: " << e.what() << endl << endl;
    param::write_usage(cout, "");
    cout << endl;
  } catch (exception& e) {
    cout << "An error has occured: " << e.what() << endl;
  }
  return sat;
}

//
// --------------------------  Local implementations --------------------------
//

// A convenience method to check if the circuit is satisfied
bool satisfied(circuit& c)
{
  return c.jfront().size() == 0;
}

// Selects and returns a gate from jfront to be justified
gate& select_from_jfront(circuit& c)
{
  const int_pqueue<jfront_cmp>& jfront = c.jfront();
  gate& g = c.gates()[jfront.top()];
#if TRACE
  cout << "picked " << g << " from jfront" << endl;
  cout << "left:" << c.gates()[abs(g.left_id())] << endl;
  cout << "right:" << c.gates()[abs(g.right_id())] << endl;
#endif
  assert (g.type() != gate::IN); // IN gates are never in jfront
  return g;
}


// Selects the gates to flip and returns their id; random_walk is set to true
// if the selection was based on random walk; this is the circuit version of
// the GWSAT heuristic.
void select_gates_gwsat(circuit& c, vector<int>& selected, bool& random_walk)
{
  selected.clear();

  // pick a gate from jfront
  gate& g = select_from_jfront(c);
  tracer::in_selection(c, g.id());

  // decide if we are doing random walk
  random_walk = (random_double() < param::wp);

  // find the set of justifications for the current truth-value of g
  int ga = g.tv() ? g.id() : -g.id();
  justifications justs = c.calculate_justifications(ga);
#if TRACE
  cout << "justifications for " << ga << " are " << justs << endl;
#endif
  assert(!justs.empty()); // not sure how could it be empty, but still ...

  // ok, we're ready to roll ...
  justification* best = 0;
  if (justs.size() == 1) {
    // if there's a single justification, we're done
#if TRACE
    cout << "justification is a singleton " << endl;
#endif
    best = &justs[0];
  } else if (random_walk) {
#if TRACE
    cout << "doing random walk " << endl;
#endif
    best = &justs[random_int(justs.size()-1)];
  } else {
#if TRACE
    cout << "doing greedy move " << endl;
#endif
    // now, find the best using heuristic to find the best justification
    vector<int> bests; // indexes of the bests
    int best_score = INT_MAX;
    for (int i = 0; i < (int)justs.size(); i++) {
      justification& j = justs[i];
#if TRACE
      cout << "checking justification " << j << endl;
#endif
      c.prepare_trial();
      // flip gates that require flipping ...
      vector<int> ids;
      ids.reserve(j.size());
      for (justification_iter iter = j.begin(); iter != j.end(); ++iter) {
        gate& g = c.gates()[abs(*iter)];
        if ((g.tv(c.tr_gen()) != (tvs)(*iter > 0)) && !g.constrained())
          ids.push_back(g.id());
      }
      if (c.multiflip(ids, true)) {
#if TRACE
        cout << "no conflict; try-score for this justification is "
             << c.objective(true)
             << endl;
#endif
        // if there's no conflict, this justification is a candidate,
        // see if it gives a better score
        long score = c.objective(true);
        if (score < best_score) {
          bests.clear();
          bests.push_back(i);
          best_score = score;
        } else if (score == best_score) {
          bests.push_back(i);
        }
      } else {
        // conflict detected - go for the next justification
#if TRACE
        cout << "conflict detected, justification is unacceptable" << endl;
#endif
      }
#if TRACE
      cout << "finished checking justification" << endl;
#endif
    }
    if (bests.empty()) {
      // this means that all minimal justifications lead to conflict, so lets
      // flip the gate itself, if possible, if not, pick a random one ...
      // -- note that since we only construct minimal justifications, we
      // cannot claim unsat here
#if TRACE
      cout << "all justifications lead to conflict, picking one"
           << endl;
#endif
      if (!g.constrained()) {
        selected.push_back(g.id());
        return;
      } else {
        best = &justs[random_int(justs.size()-1)];
      }
    } else {
      // best is random from the bests ...
      best = &justs[bests[random_int(bests.size()-1)]];
    }
  }
  // populate selected from the best
#if TRACE
  cout << "best gates: " << *best << endl;
#endif
  for (justification_iter iter = best->begin(); iter != best->end(); ++iter) {
    gate& g = c.gates()[abs(*iter)];
    if (!g.constrained() && (g.tv() != (tvs)(*iter > 0))) // note the real tv here
      selected.push_back(g.id());
  }
  return;
}

// SIGALRM handler for timeouts
//
void sigalrm_handler(int sig)
{
#if TRACE
  cout << "Timeout !" << endl;
#endif
  timeout = true;
}
