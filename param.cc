/*
 Copyright (c) 2009-2011, Anton Belov, anton.belov@ucd.ie
 */

// ----------------------------------------------------------------------------
//
// param.cc - implementation of the parameters parsing/retrieving functions.
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//

#include <cassert>
#include <getopt.h>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include "param.h"

using namespace std;
using namespace param;

//
// -----------------------  Globally available data ------------------------
//

int      param::tries = 10;         // the number of tries
int      param::cutoff = 100000;    // cutoff
float    param::wp = 0.05f;         // walk probability
int      param::verbose = 3;        // verbosity level
bool     param::rld = false;        // true if RLD/RTD is required
string   param::input_file;         // input file path
bool     param::print_sat_assign = false; // true if assignment is needed
int      param::rn_seed = 0;        // the random seed
int      param::rn_state = 0;       // the state for random generator
bool     param::trace = false;      // true if tracer should be enabled
bool     param::dump_gml = false;   // if true, write graphml and exit
unsigned param::timeout = 0;        // timeout per try (secs) or 0 if none
bool     param::dump_stats = false; // if true, dump instance stats and exit


//
// ---------------------------  Local declarations --------------------------
//

//
// ------------------------  Exported implementations -----------------------
//

// Called from main to parse the parameters. Throws param::parse_error in case
// of problems
//
void param::init(int argc, char *argv[]) throw (param::parse_error)
{
  // the specification for parameters
  struct option options[] = {
    { "tries", required_argument, NULL, 0 },     // 0
    { "cutoff", required_argument, NULL, 0 },    // 1
    { "wp", required_argument, NULL, 0 },        // 2
    { "verbose", required_argument, NULL, 0 },   // 3
    { "rld", required_argument, NULL, 0 },       // 4
    { "print", required_argument, NULL, 0 },     // 5
    { "rnseed", required_argument, NULL, 0 },    // 6
    { "rnstate", required_argument, NULL, 0 },   // 7
    { "trace", required_argument, NULL, 0},      // 8
    { "dumpgml", required_argument, NULL, 0},    // 9
    { "timeout", required_argument, NULL, 0},    // 10
    { "dumpstats", required_argument, NULL, 0},  // 11
    { 0, 0, 0, 0 } };

  int index, c;

  // no messages, we will do them ourselves
  opterr = 0;

  // pick up the given agruments
  while ((c = getopt_long_only(argc, argv, "", options, &index)) >= 0) {
    if (c == '?') {
      std::ostringstream os;
      os << "unrecognized character (" << optopt << ")";
      throw parse_error(os.str());
    }
    std::istringstream in(optarg);
    // figure out which option was given, based on index
    switch (index) {
    case 0:
      in >> tries; break;
    case 1:
      in >> cutoff; break;
    case 2:
      in >> wp; break;
    case 3:
      in >> verbose; break;
    case 4:
      in >> rld; break;
    case 5:
      in >> print_sat_assign; break;
    case 6:
      in >> rn_seed; break;
    case 7:
      in >> rn_state; break;
    case 8:
      in >> trace; break;
    case 9:
      in >> dump_gml; break;
    case 10:
      in >> timeout; break;
    case 11:
      in >> dump_stats; break;
    default:
      assert(false); // shouldn't be here
    }
  }
  // the only thing that must've left over is the input file name
  if (optind >= argc)
    throw parse_error("input file name is missing");
  input_file.assign(argv[optind]);
  // all done
}

// Writes out the parameters to the specified output stream
void param::write(ostream& out, string prefix)
{
  out << prefix << "input file name = " << input_file << endl;
  out << prefix << "tries = " << tries << endl;
  out << prefix << "cutoff = " << cutoff << endl;
  out << prefix << "wp = " << wp << endl;
  out << prefix << "rnseed = " << rn_seed<< endl;
  out << prefix << "rnstate = " << rn_state<< endl;
  out << prefix << "timeout = " << timeout << endl;
  char hostname[1024];
  if (gethostname(hostname, 1024) == 0)
    out << prefix << "hostname = " << hostname << endl;
}

// Writes out the usage to the specified output stream
void param::write_usage(ostream& out, string prefix) 
{
  out << prefix << "Usage: crsat [options] <input-file-name>" << endl;
  out << prefix << "    " << endl;
  out << prefix << "    <input-file-name> -- must be a binary AIG (i.e. .aig, but not .aag);" << endl;
  out << prefix << "                         maybe GZIP compressed or not (ends with .aig or .aig.gz)" << endl;
  out << prefix << "Options:"     << endl;
  out << prefix << "    -tries=<int>   -- number of tries" << endl;
  out << prefix << "    -cutoff=<int>  -- max steps per try" << endl;
  out << prefix << "    -wp=<float>    -- probability of random walk [0.0 ... 1.0]" << endl;
  out << prefix << "    -v=<0|1|2>     -- verbosity level" << endl;
  out << prefix << "    -rld=<0|1>     -- if 1, print RLD and RTD after the run" << endl;
  out << prefix << "    -print=<0|1>   -- if 1, print SAT assignment, if found" << endl;
  out << prefix << "    -rnseed=<int>  -- if >0 the value is used as a random seed" << endl;
  out << prefix << "                      if =0 the seed is made from the date and time" << endl;
  out << prefix << "                      if <0 the value is igonored, and pseudorandom generator" << endl;
  out << prefix << "                      is brought into state specified by -rnstate parameter" << endl;
  out << prefix << "    -rnstate=<int> -- if -rnseed is <0, then this value is used as state for" << endl;
  out << prefix << "                      pseudorandom generator" << endl;
  out << prefix << "    -trace=<0|1>   -- if 1, then a detailed trace of execution is written into" << endl;
  out << prefix << "                      a .trace file in the current directory" << endl;
  out << prefix << "    -timeout=<int> -- timeout to be imposed on each try [secs]" << endl;
  out << prefix << "    " << endl;
  out << prefix << "Other usages:" << endl;
  out << prefix << "    crsat -dumpgml=1 <input-file-name> -- writes out circuit graph in GraphML format" << endl;
  out << prefix << "                                         and exits." << endl;
  out << prefix << "    crsat -dumpstats=1 <input-file-name> -- prints circuit statistics and exits." << endl;
}
