/*
 Copyright (c) 2009-2011, Anton Belov, anton.belov@ucd.ie
 */

// ----------------------------------------------------------------------------
//
// param.h - solver parameters declarations
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//

#ifndef _PARAM_H_
#define _PARAM_H_

#include <ostream>
#include <string>

namespace param
{

//
// -------------------------  Datastructures and types ------------------------
//

// Exception for parameter parsing errors
class parse_error {
 public:
 parse_error() : _what("") {}
 parse_error(const std::string& what) : _what(what) {}
  std::string& what() { return _what; }
 private:
  std::string _what;
};

//
// ------------------------  Global variable declarations ---------------------
//

extern int          tries;              // the number of tries
extern int          cutoff;             // cutoff
extern float        wp;                 // walk probability
extern std::string  input_file;         // input file path
extern bool         rld;                // true if RLD/RTD is required
extern int          verbose;            // verbosity level
extern bool         print_sat_assign;   // true is assignment is needed
extern int          rn_seed;            // the random seed
extern int          rn_state;           // the state for random generator
extern bool         trace;              // true if tracer should be enabled
extern bool         dump_gml;           // if true, write graphml and exit
extern unsigned int timeout;            // timeout per try (secs) or 0 if none
extern bool         dump_stats;         // if true, dump instance stats and exit

//
// ---------------------------  Inline non-members ----------------------------
//

//
// --------------------------  Outline non-members ----------------------------
//

// Initializes the parameters from command line
void init(int argc, char *argv[]) throw(parse_error);

// Writes out the parameters to the specified output stream
void write(std::ostream& out, std::string prefix = "# ");

// Writes out the usage to the specified output stream
void write_usage(std::ostream& out, std::string prefix = "");

}

#endif /* _PARAM_H_ */

