/*
 Copyright (c) 2009-2011, Anton Belov, anton.belov@ucd.ie
 */

// ----------------------------------------------------------------------------
//
// stats.cc - implementation of the statistics capture and reporting
//            functions.
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//

#include <cassert>
#include <climits>
#include <cstdlib>
#include <iostream>
#include <map>
#include <sys/times.h>
#include <unistd.h>
#include "circuit.h"
#include "param.h"
#include "stats.h"
#include "utils.h"

extern "C" double sqrt(double); // from <math.h>

using std::map;
using std::cout;
using std::endl;

// the descriptive statistics maintainer - argument is the max number of
// datasets ...
stats::descr_stats stats::ds(100);

#ifndef NOSTATS  // define NOSTATS to make everything a noop

//
// ---------------------------  Local declarations -------------------------
//

// per-try statistics data structure
static struct _try_stats {
  clock_t       start_ts;        // timestamp at the begining of the run
  int           init_jfs;        // initial jfront size
  int           best_jfs;        // best jfront size
  int           best_jfs_step;   // best jfront step
  long          jfs_acc;         // accumulator for avg jfront size
  long          init_obj;        // initial interest size
  long          best_obj;        // best interest size
  long          best_obj_step;   // best interest step
  long          obj_acc;         // accumulator for avg interest size
  int	        rwalks;		     // count of random walks
  int           num_flips;       // count of flips
  long          num_props_up;    // count of upward propagations
  long          num_props_down;  // count of downward propagations
  long          num_props_side;  // count of side propagations
  long          num_confls_up;    // count of upward conflicts
  long          num_confls_down;  // count of upward conflicts
  long          num_confls_side;  // count of upward conflicts
  long          num_bcp_prop_deqs;// count of gates poped from propagation
                                  // queue
  long          num_bcp_state_deqs;//count of gates poped from state update
                                   // queue
} try_stats = { 0 };

// global statistics data structure
static struct _glob_stats {
  int           num_tries;		 // total number of tries
  long          num_steps;       // total number of steps
  long          num_flips;       // total number of flips
  int           num_sat;         // number of times got solved
  long          num_sat_steps;   // total number of steps when got solved
  long   	    elapsed_time_sat;// total time when got solved
  int           sps_samples;     // the number of valid sps samples
  long          sps_acc;         // the accumulated sps
  float         jfs_acc;         // the accumulated avg jfs
  int           best_jfs_acc;    // the accumulated best jfs
  int           init_jfs_acc;    // the accumulated initial jfs
  int           jfs_samples;     // the number of valid samples for jfs
  int           *rld_data;       // keeps the data for RLD
  int           *rtd_data;       // keeps the data for RTD
  long          rwalks_acc;		   // accumulator for rwalks
  long          props_acc;       // accumulator for propagations
  long          confls_acc;      // accumulator for conflicts
} glob_stats = { 0 };

// the ticks per second constant
static long TCK_S;

// A comparator function for integers (for sorting)
static int intcmp(const void *a, const void *b);

//
// ------------------------  Exported implementations ----------------------
//


// To be called immediately after the parameters has been parsed
void stats::after_params(void)
{
  // configure the clock
  TCK_S = sysconf(_SC_CLK_TCK); // ticks per second
  if (param::verbose > 0)
    param::write(std::cout);
}


// To be called before parsing the circuit
void stats::before_input(void)
{
}


// To be called after parsing the circuit
void stats::after_input(circuit& c)
{
  if (param::verbose > 1) {
    printf("# circuit parameters\n");
    printf("# gates ins outs constrs\n");
    printf("%d %d %d %d\n", c.num_gates(), c.num_ins(), c.num_outs(), c.num_constrs());
  }
}


// To be called after preprocessing of the circuit
void stats::after_preprocessing(circuit& c)
{
  if (param::verbose > 1) {
    printf("# after preprocessing\n");
    printf("# constrs unreach depth");
    printf("\n%d %d %d", c.num_constrs(), c.num_unreach(), c.depth());
    printf("\n");
  }
}

// Writes out instance stats on one line - call after preprocessing
void stats::write_circuit_stats(circuit& c)
{
  if (param::verbose > 1) {
    printf("# instance stats: ");
    printf("gates=%d ", c.num_gates());
    printf("ins=%d ", c.num_ins());
    printf("outs=%d ", c.num_outs());
    printf("constrs_after_prep=%d ", c.num_constrs());
    printf("depth=%d ", c.depth());
    printf("\n");
  }
}

// To be called immediately before the main loop starts
void stats::before_main_loop(circuit& c)
{
  if (param::verbose > 1) {
    printf("# tries\n");
    printf("# try found steps time(ms) sps jfs_init jfs_best jfs_avg jfs_best_step obj_init obj_best obj_avg obj_best_step rwalks flips props_up props_down props_side confls_up confls_down confls_side rn_state bcp_prop_deqs bcp_state_deqs\n");
  }    
  glob_stats.rld_data = new int[param::tries];
  glob_stats.rtd_data = new int[param::tries];
}


// To be called at the begining of each try right after the initial assignment has been made.
void stats::before_try(circuit& c, int try_num)
{
  struct tms tinfo;

  // initialize try stats
  
  // Note regarding the times: all solvers out there measure user time
  // only - I'm not sure its fair, but ok.
  times(&tinfo);
  try_stats.start_ts = tinfo.tms_utime; // + tinfo.tms_stime;
  try_stats.init_jfs = -1;
  try_stats.best_jfs = INT_MAX;
  try_stats.best_jfs_step = 0;
  try_stats.jfs_acc = 0;
  try_stats.init_obj = -1;
  try_stats.best_obj = LONG_MAX;
  try_stats.best_obj_step = 0;
  try_stats.obj_acc = 0;
  try_stats.rwalks = 0;
  try_stats.num_flips = 0;
  try_stats.num_props_up = 0;
  try_stats.num_props_down = 0;
  try_stats.num_props_side = 0;
  try_stats.num_confls_up = 0;
  try_stats.num_confls_down = 0;
  try_stats.num_confls_side = 0;
  try_stats.num_bcp_prop_deqs = 0;
  try_stats.num_bcp_state_deqs = 0;
}

// To be called before each step. The variables to flip are already calculated
void stats::before_step(circuit& c, int step_num, int num_selected)
{
  try_stats.num_flips += num_selected;
}

// To be called after each step.
void stats::after_step(circuit& c, int step_num, bool random_walk)
{
  if (random_walk)
    try_stats.rwalks++;
  
  int jfs = c.jfront().size();
  if (try_stats.init_jfs < 0)
    try_stats.init_jfs = jfs;
  if (jfs < try_stats.best_jfs) {
    try_stats.best_jfs = jfs;
    try_stats.best_jfs_step = step_num;
  }
  try_stats.jfs_acc += jfs;
  
  int obj = c.objective();
  if (try_stats.init_obj < 0)
    try_stats.init_obj = obj;
  if (obj < try_stats.best_obj) {
    try_stats.best_obj = obj;
    try_stats.best_obj_step = step_num;
  }
  try_stats.obj_acc += obj;
}


// To be called at the end of each try - either when the solution is found or the
// maximum number of flips has been exceeded
void stats::after_try(circuit& c, int try_num, int num_steps)
{
  struct tms tinfo;
  int solved;
  int elapsed_t; // elapsed time in millis
  int sps = 0;
  float avg_jfs = -1;
  float avg_is = -1;
  
  glob_stats.num_tries++;
  times(&tinfo);
  elapsed_t = (int)((1000*(double)(tinfo.tms_utime // + tinfo.tms_stime
                                   - try_stats.start_ts))/TCK_S);
  glob_stats.num_steps += num_steps;
  glob_stats.num_flips += try_stats.num_flips;
  solved = c.calculate_status();
  if (solved) {
    glob_stats.num_sat++;
    glob_stats.num_sat_steps += num_steps;
    glob_stats.elapsed_time_sat += elapsed_t;
  }
  if (elapsed_t > 0) {
    sps = (int)((double)num_steps*1000/elapsed_t);
    glob_stats.sps_samples++;
    glob_stats.sps_acc += sps;
  }
  if (num_steps > 0) {
    avg_jfs = (float)try_stats.jfs_acc/num_steps;
    glob_stats.jfs_acc += avg_jfs;
    glob_stats.best_jfs_acc += try_stats.best_jfs;
    glob_stats.init_jfs_acc += try_stats.init_jfs;
    glob_stats.jfs_samples++;
    avg_is = (float)try_stats.obj_acc/num_steps;
  }
  glob_stats.rld_data[try_num] = (solved) ? num_steps : INT_MAX;
  glob_stats.rtd_data[try_num] = elapsed_t;
  glob_stats.rwalks_acc += try_stats.rwalks;
  glob_stats.props_acc += try_stats.num_props_up + try_stats.num_props_down +
    try_stats.num_props_side;
  glob_stats.confls_acc += try_stats.num_confls_up + 
    try_stats.num_confls_down + try_stats.num_confls_side;

  // print out
  if (param::verbose > 1) {
    printf("%d ", try_num+1);
    printf("%d ", solved);
    printf("%d ", num_steps);
    printf("%d ", (int)elapsed_t);
    printf("%d ", (sps > 0) ? sps : -1);
    if (avg_jfs >= 0)
      printf("%d %d %.1f ", try_stats.init_jfs, try_stats.best_jfs, avg_jfs);
    else
      printf("-1 -1 -1 ");
    printf("%d ", try_stats.best_jfs_step);
    if (avg_is >= 0)
      printf("%ld %ld %.1f ", try_stats.init_obj, try_stats.best_obj, avg_is);
    else
      printf("-1 -1 -1 ");
    printf("%ld ", try_stats.best_obj_step);
    printf("%d ", try_stats.rwalks);
    printf("%d ", try_stats.num_flips);
    printf("%ld ", try_stats.num_props_up);
    printf("%ld ", try_stats.num_props_down);
    printf("%ld ", try_stats.num_props_side);
    printf("%ld ", try_stats.num_confls_up);
    printf("%ld ", try_stats.num_confls_down);
    printf("%ld ", try_stats.num_confls_side);
    printf("%d ", peek_random());
    printf("%ld ", try_stats.num_bcp_prop_deqs);
    printf("%ld ", try_stats.num_bcp_state_deqs);
    printf("\n");
  }
}

// To be called at the end of the main loop, i.e. when the desired number of tries
// has been performed
void stats::after_main_loop(circuit& c)
{
  int tries = glob_stats.num_tries;
  int cutoff = param::cutoff;
  int is_sat = 0;
  float succ_rate = -1;
  float avg_steps_till_sat = -1;
  float avg_fps = -1;
  float avg_jfs = -1;
  float avg_best_jfs = -1;
  float avg_init_jfs = -1;
  float avg_time_till_sat = -1;
  float avg_rwalks = -1;
  float avg_steps = -1;
  float med_steps = -1;
  float stdev_steps = -1;
  float smr_steps = -1;
  float avg_time = -1;
  float med_time = -1;
  float stdev_time = -1;
  float smr_time = -1;
  float props_per_step = -1;
  float confls_per_step = -1;
  int nf, time, i;
  
  is_sat = glob_stats.num_sat;
  if (glob_stats.num_sat > 0) {
    avg_steps_till_sat = (float)glob_stats.num_sat_steps/glob_stats.num_sat;
    avg_time_till_sat = (float)glob_stats.elapsed_time_sat
      /glob_stats.num_sat;
  }
  if (glob_stats.sps_samples > 0)
    avg_fps = (float)glob_stats.sps_acc/glob_stats.sps_samples;
  if (glob_stats.jfs_samples > 0) {
    avg_jfs = (float)glob_stats.jfs_acc/glob_stats.jfs_samples;
    avg_best_jfs = (float)glob_stats.best_jfs_acc/glob_stats.jfs_samples;
    avg_init_jfs = (float)glob_stats.init_jfs_acc/glob_stats.jfs_samples;
  }
  if (tries > 0) {
    succ_rate = (float)glob_stats.num_sat*100/tries;
    avg_rwalks = (float)glob_stats.rwalks_acc/tries;
    props_per_step = (float)glob_stats.props_acc/glob_stats.num_steps;
    confls_per_step = (float)glob_stats.confls_acc/glob_stats.num_steps;
    
    // process RTD/RLD
    qsort(glob_stats.rld_data, tries, sizeof(int), intcmp);
    qsort(glob_stats.rtd_data, tries, sizeof(int), intcmp);
    avg_steps = med_steps = stdev_steps = 0;
    avg_time = med_time = stdev_time = 0;
    for (i = 0; i < tries; i++) {
      avg_steps+= (glob_stats.rld_data[i] != INT_MAX) ? glob_stats.rld_data[i] : cutoff;
      avg_time += glob_stats.rtd_data[i];
    }
    avg_steps /= tries;
    avg_time /= tries;
    if (tries % 2) {
      int	val = (glob_stats.rld_data[tries/2] != INT_MAX) ? glob_stats.rld_data[tries/2] : cutoff;
      med_steps = val;
      med_time = glob_stats.rtd_data[tries/2];
    } else {
      int	val1 = (glob_stats.rld_data[tries/2-1] != INT_MAX) ? glob_stats.rld_data[tries/2-1] : cutoff;
      int	val2 = (glob_stats.rld_data[tries/2] != INT_MAX) ? glob_stats.rld_data[tries/2] : cutoff;
      med_steps = ((float)val1 + val2)/2;
      med_time = ((float)glob_stats.rtd_data[tries/2-1] + glob_stats.rtd_data[tries/2])/2;
    }
    for (i = 0; i < tries; i++) {
      float diff = avg_steps - ((glob_stats.rld_data[i] != INT_MAX) ? glob_stats.rld_data[i] : cutoff);
      stdev_steps += diff*diff;
      diff = avg_time - glob_stats.rtd_data[i];
      stdev_time += diff*diff;
    }
    stdev_steps = sqrt(stdev_steps/tries);
    smr_steps = stdev_steps/avg_steps;
    stdev_time = sqrt(stdev_time/tries);
    smr_time = stdev_time/avg_time;
  }
  // print out results
  if (param::verbose > 0) { 
    printf("# run\n");
    printf("# succ_rate "
           "med_steps "
           "avg_steps "
           "stdev_steps "
           "stdev_avg_steps "
           "med_time "
           "avg_time "
           "stdev_time "
           "stdev_avg_time "
           "avg_steps_succ "
           "avg_time_succ "
           "avg_sps "
           "avg_init_jfs "
           "avg_best_jfs "
           "avg_jfs "
           "avg_rwalk "
           "props_p_step "
           "confls_p_step "
           "\n");
    printf("%.2f ", succ_rate);
    printf("%.2f ", med_steps);
    printf("%.2f ", avg_steps);
    printf("%.2f ", stdev_steps);
    printf("%.2f ", smr_steps);
    printf("%.2f ", med_time);
    printf("%.2f ", avg_time);
    printf("%.2f ", stdev_time);
    printf("%.2f ", smr_time);
    printf("%.2f ", avg_steps_till_sat);
    printf("%.2f ", avg_time_till_sat);
    printf("%.2f ", avg_fps);
    printf("%.2f ", avg_init_jfs);
    printf("%.2f ", avg_best_jfs);
    printf("%.2f ", avg_jfs);
    printf("%.2f ", avg_rwalks);
    printf("%.2f ", props_per_step);
    printf("%.2f ", confls_per_step);
    printf("\n");
  }

  if (param::rld) {
    // RLD data
    printf("# rld\n");
    printf("# p(solved), steps\n");
  
    for (i = 0; i < tries; i++) {
      nf = glob_stats.rld_data[i];
      if (nf == INT_MAX)
        break;
      printf("%.5f, %d\n", (float)(i+1)/tries, nf);
    }
    
    // RTD data
    printf("# rtd\n");
    printf("# p(solved), time(ms)\n");
    for (int j = 0; j < i; j++) {
      time = glob_stats.rtd_data[j];
      printf("%.5f, %d\n", (float)(j+1)/tries, time);
    }
  }
  // free the memory allocated for RLD data
  delete[] glob_stats.rld_data;
  delete[] glob_stats.rtd_data;
}


// To be called after a single truth-value has been assigned in propagation
void stats::after_prop(int direction)
{
  switch (direction) {
  case 0: try_stats.num_props_up++; break;
  case 1: try_stats.num_props_down++; break;
  case 2: try_stats.num_props_side++; break;
  }
}

// To be called after a conflict has been detected during propagation
void stats::after_conflict(int direction)
{
  switch (direction) {
  case 0: try_stats.num_confls_up++; break;
  case 1: try_stats.num_confls_down++; break;
  case 2: try_stats.num_confls_side++; break;
  }
}

// To be called whenever a gate is pulled from BCP propagation queue
void stats::after_bcp_prop_dequeue(void)
{
  try_stats.num_bcp_prop_deqs++;
}

// To be called whenever a gate is pulled from BCP state update queue
void stats::after_bcp_state_dequeue(void)
{
  try_stats.num_bcp_state_deqs++;
}

// Only for hackers ...
void stats::get_structs(void*& try_stats_addr, void*& glob_stats_addr)
{
  try_stats_addr = &try_stats;
  glob_stats_addr = &glob_stats;
}


// --------------------------  Local implementations ----------------------- 
//                                                                           

/* A comparator function for integers (used for sorting). Returns -1/0/1 iff a</=/>b */
static int intcmp(const void *a, const void *b)
{
  int ai = *(int *)a;
  int bi = *(int *)b;
  return ((ai < bi) ? -1 : ((ai == bi) ? 0 : 1));
}

#endif // NOSTATS
