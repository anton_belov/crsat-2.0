/*
 Copyright (c) 2009-2011, Anton Belov, anton.belov@ucd.ie
 */

// ----------------------------------------------------------------------------
//
// stats.h - declaration of the statistics capture and reporting functions.
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

#include <assert.h>
#include <limits.h>
#include <map>
#include <stdlib.h>
#include <sys/times.h>
#include <vector>
#include <unistd.h>
#include "circuit.h"

#ifndef _STATS_H_
#define _STATS_H_

extern "C" double sqrt(double); // from <math.h>

namespace stats
{

//
// -------------------------  Datastructures and types ------------------------
//

#ifndef NOSTATS // define NOSTATS to make everything noop

// This is a cool little class that is used for calculation of various
// descriptive statistics (mean and standard deviation currently) on the
// fly. The data sets are keyed by an integer key.
class descr_stats
{
private:
  // this is the data that is kept for a single dataset
  struct _descr_stats {
    double mean;
    double variance;
    double min;
    double max;
    int num_samples;
    _descr_stats() : mean(0), variance(0), min(0), max(0) {}
    void clear(void) { mean = variance = min = max = 0; num_samples = 0; }
  };
  // this is where the data for all datasets is kept
  std::vector<_descr_stats> _data;

public:

  // constructor; num_keys is the maximum number of datasets supported
  descr_stats(int num_keys) : _data(num_keys) {};

  // clears all the data for the specified dataset
  void clear(int key) { _data[key].clear(); }
  // adds a sample point to a dataset
  void add_sample(int key, double sample) {
    _descr_stats& s = _data[key];
    // we're using an online algorithm for mean and variance (due to Knuth, see
    // wikipedia article on the subject for details)
    s.num_samples++;
    double delta = sample - s.mean;
    s.mean += delta/s.num_samples;
    s.variance += delta*(sample - s.mean);
    if (s.num_samples == 1) {
      s.min = s.max = sample;
    } else {
      if (sample < s.min)
        s.min = sample;
      if (sample > s.max)
        s.max = sample;
    }
  }
  // returns the number of sample points in the dataset
  int get_num_samples(int key) { return _data[key].num_samples; }
  // returns the current mean of the dataset
  double get_mean(int key) { return _data[key].mean; }
  // returns the current variance of the dataset
  double get_variance(int key) {
    return _data[key].variance/(_data[key].num_samples - 1);
  }
  // returns the current standard deviation of the dataset
  double get_stdev(int key) { return sqrt(get_variance(key)); }
  // returns the current minimum value (works correctly when num_samples > 0)
  double get_min(int key) { return _data[key].min; }
  // returns the current maximum value (works correctly when num_samples > 0)
  double get_max(int key) { return _data[key].max; }
};

#else

class descr_stats
{
public:
  descr_stats(int num_keys) {}
  void clear(int key) {}
  void add_sample(int key, double sample) {}
  int get_num_samples(int key) { return 0; }
  double get_mean(int key) { return 0; }
  double get_variance(int key) { return 0; }
  double get_stdev(int key) { return 0; }
  double get_min(int key) { return 0; }
  double get_max(int key) { return 0; }
};

#endif // NOSTATS

//
// ------------------------  Global variable declarations ---------------------
//

// the descriptive statistics maintainer
extern descr_stats ds;

//
// ---------------------------  Inline non-members ----------------------------
//

//
// --------------------------  Outline non-members ----------------------------
//

#ifndef NOSTATS // define NOSTATS to make everything noop

// To be called immediately after the parameters has been parsed
void after_params(void);
// To be called before parsing the circuit
void before_input(void);
// To be called after parsing the circuit
void after_input(circuit& c);
// To be called after preprocessing of the circuit
void after_preprocessing(circuit& c);
// Writes out instance stats on one line - call after preprocessing
void write_circuit_stats(circuit& c);
// To be called immediately before the main loop starts
void before_main_loop(circuit& c);
// To be called at the begining of each try right after the initial assignment
// has been made
void before_try(circuit& c, int try_num);
// To be called before each step. The gates to flip are already calculated
void before_step(circuit& c, int step_num, int num_selected);
// To be called after each step
void after_step(circuit& c, int step_num, bool random_walk);
// To be called at the end of each try - either when the solution is found or
// the maximum number of flips has been exceeded.
void after_try(circuit& c, int try_num, int num_steps);
// To be called at the end of the main loop, i.e. when the desired number of
// tries has been performed.
void after_main_loop(circuit& c);
// To be called after a single truth-value has been assigned in propagation;
// direction is 0 for upward (to parent), 1 for downward (to children), 2 for
// side (to sibling)
void after_prop(int direction = 0);
// To be called after a conflict has been detected during propagation;
// direction is 0 for upward (to parent), 1 for downward (to children), 2 for
// side (to sibling)
void after_conflict(int direction = 0);
// To be called whenever a gate is pulled from BCP propagation queue
void after_bcp_prop_dequeue(void);
// To be called whenever a gate is pulled from BCP state update queue
void after_bcp_state_dequeue(void);
// Only for hackers ...
void get_structs(void*& try_stats_addr, void*& glob_stats_addr);

#else

inline void after_params(void) {}
inline void before_input(void) {}
inline void after_input(circuit& c) {}
inline void after_preprocessing(circuit& c) {}
inline void write_circuit_stats(circuit& c) {}
inline void before_main_loop(circuit& c) {}
inline void before_try(circuit& c, int try_num) {}
inline void before_step(circuit& c, int step_num, int num_selected) {}
inline void after_step(circuit& c, int step_num, bool random_walk) {}
inline void after_try(circuit& c, int try_num, int num_steps) {}
inline void after_main_loop(circuit& c) {}
inline void after_prop(int direction = 0) {}
inline void after_conflict(int direction = 0) {}
inline void after_bcp_prop_dequeue(void) {}
inline void after_bcp_state_dequeue(void) {}

#endif // NOSTATS

}

#endif // _STATS_H_

