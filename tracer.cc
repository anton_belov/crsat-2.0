/*
 Copyright (c) 2009-2010, Anton Belov, antonb@cse.yorku.ca
 */

// ----------------------------------------------------------------------------
//
// tracer.cc - implementation of the trace functionality.
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//

#include <cassert>
#include <iostream>
#include <fstream>
#include "circuit.h"
#include "param.h"
#include "tracer.h"
#include "utils.h"

using namespace std;

#ifndef NOTRACER // define NOTRACER to make everything a noop

//
// ---------------------------  Local declarations --------------------------
//
namespace {

  // the output stream for the tracer
  ofstream fout;

}

//
// ------------------------  Exported implementations ----------------------
//


// To be called immediately before the main loop starts
void tracer::before_main_loop_real(const circuit& c)
{
  // open up the file for tracing ...
  size_t pos = param::input_file.rfind('/');
  string iname = param::input_file.
      substr((pos == string::npos) ? 0 : pos + 1).
      append(".trace");
  fout.open(iname.c_str());
  if (!fout) {
    cerr << "Error: unable to open " << iname
         << " for writing, either fix this or run with -trace=0." << endl;
    exit(-1);
  }
  // dump parameters
  param::write(fout);
}

// To be called at the beginning of each try right after the initial assignment
// has been made.
void tracer::before_try_real(const circuit& c, int try_num)
{
  fout << "+try " << try_num << endl;
  // dump the circuit ...
  for (int i = 1; i <= c.num_gates(); i++) {
    gate& g = c.gates()[i];
    fout << "i"
      << " " << g.id()
      << " " << g.tv()
      << " " << g.just()
      << " " << g.objective()
      << endl;
  }
}

// To be called at the begining of the step before selection is done
void tracer::before_selection_real(const circuit& c, int step_num)
{
  fout << "+step " << step_num << endl;
}

// To be called during the selection when the unjust gate is known
void tracer::in_selection_real(const circuit& c, int selected_unjust)
{
  fout << "s " << selected_unjust << endl;
}

// To be called before each step. The variables to flip are already calculated
void tracer::before_step_real(const circuit& c, int step_num, const vector<int>& selected)
{
  // we're piggy backing gates that were selected as those that have not changed
  for (int i = 0; i < (int)selected.size(); i++)
    fout << "s " << selected[i] << endl;
}

// To be called during the step for each gate whose state changes
void tracer::after_gate_state_change_real(const gate& g,
    tvs old_tv, bool old_just, long old_objective)
{
  fout << "c"
    << " " << g.id()
    << " " << (g.tv() - old_tv)
    << " " << (g.just() - old_just)
    << " " << (g.objective() - old_objective)
    << endl;
}

// To be called after each step.
void tracer::after_step_real(const circuit& c, int step_num)
{
  fout << "-step " << step_num << endl;
}

// To be called at the end of each try - either when the solution is found or the
// maximum number of flips has been exceeded
void tracer::after_try_real(const circuit& c, int try_num, int num_steps)
{
  fout << "-try " << try_num << endl;
}

// To be called at the end of the main loop, i.e. when the desired number of
// tries has been performed.
void tracer::after_main_loop_real(const circuit& c)
{
  fout.close();
}

//                                                                           
// --------------------------  Local implementations ----------------------- 
//                                                                           

#endif // NOTRACER
