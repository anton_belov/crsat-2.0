/*
 Copyright (c) 2009-2010, Anton Belov, antonb@cse.yorku.ca
 */

// ----------------------------------------------------------------------------
//
// tracer.h - declaration of the trace functionality
//
// Anton Belov, antonb@cse.yorku.ca, June 2010 - ...
//
// ----------------------------------------------------------------------------

#include <assert.h>
#include <limits.h>
#include <map>
#include <stdlib.h>
#include <sys/times.h>
#include <vector>
#include <unistd.h>
#include "circuit.h"
#include "param.h"

#ifndef _TRACER_H_
#define _TRACER_H_

namespace tracer
{

//
// -------------------------  Datastructures and types ------------------------
//

//
// ------------------------  Global variable declarations ---------------------
//

//
// ---------------------------  Inline non-members ----------------------------
//

//
// --------------------------  Outline non-members ----------------------------
//

#ifndef NOTRACER // define NOTRACER to make everything noop

//
// real implementations in tracer.cc -- called only if param::trace is true
//
void before_main_loop_real(const circuit& c);
void before_try_real(const circuit& c, int try_num);
void in_selection_real(const circuit& c, int selected_unjust);
void before_selection_real(const circuit& c, int step_num);
void before_step_real(
    const circuit& c, int step_num, const std::vector<int>& selected);
void after_gate_state_change_real(
    const gate& g, tvs old_tv, bool old_just, long old_objective);
void after_step_real(const circuit& c, int step_num);
void after_try_real(const circuit& c, int try_num, int num_steps);
void after_main_loop_real(const circuit& c);

// To be called immediately before the main loop starts
inline void before_main_loop(const circuit& c) {
  if (param::trace) before_main_loop_real(c);
}
// To be called at the beginning of each try right after the initial assignment
// has been made.
inline void before_try(const circuit& c, int try_num) {
  if (param::trace) before_try_real(c, try_num);
}
// To be called at the begining of the step before selection is done
inline void before_selection(const circuit& c, int step_num) {
  if (param::trace) before_selection_real(c, step_num);
}
// To be called during the selection when the unjust gate is known
inline void in_selection(const circuit& c, int selected_unjust) {
  if (param::trace) in_selection_real(c, selected_unjust);
}
// To be called before each step. The gates to flip are already calculated
inline void before_step(
    const circuit& c, int step_num, const std::vector<int>& selected) {
  if (param::trace) before_step_real(c, step_num, selected);
}
// To be called during the step for each gate whose state changes
inline void after_gate_state_change(
    const gate& g, tvs old_tv, bool old_just, long old_objective) {
  if (param::trace) after_gate_state_change_real(g, old_tv, old_just, old_objective);
}
// To be called after each step
inline void after_step(const circuit& c, int step_num) {
  if (param::trace) after_step_real(c, step_num);
}
// To be called at the end of each try - either when the solution is found or
// the maximum number of flips has been exceeded.
inline void after_try(const circuit& c, int try_num, int num_steps) {
  if (param::trace) tracer::after_try_real(c, try_num, num_steps);
}
// To be called at the end of the main loop, i.e. when the desired number of
// tries has been performed.
inline void after_main_loop(const circuit& c) {
  if (param::trace) after_main_loop_real(c);
}

#else

inline void before_main_loop(const circuit& c);
inline void before_try(const circuit& c, int try_num) {}
inline void before_selection(const circuit& c, int step_num) {}
inline void in_selection(const circuit& c, int selected_unjust) {}
inline void before_step(
    const circuit& c, int step_num, const std::vector<int>& selected) {}
inline void after_gate_state_change(
    const gate& g, tvs old_tv, bool old_just, long old_objective) {}
inline void after_step(const circuit& c, int step_num) {}
inline void after_try(const circuit& c, int try_num, int num_steps) {}
inline void after_main_loop(const circuit& c);

#endif // NOTRACER

}

#endif // _TRACER_H_

