// ----------------------------------------------------------------------------
//
// utils.cc - implementation of various utitily classes and functions.
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//

#include <cstdlib>
#include <sys/times.h>
#include "utils.h"

//
// ---------------------------  Global declarations -------------------------
//

//
// ---------------------------  Local declarations -------------------------
//
namespace {

// this array is used to keep the state of the random generator
int prg_state[2];

}

//
// ------------------------  Exported implementations ----------------------
//

// Initializes random generator. If seed>=0, then the generator is initialized
// with this value of seed, and the last_rn parameter is ignored. If seed<0,
// and then if last_rn=0 the current time is used to initialize the generator,
// otherwise (i.e.e seed<0 and last_rn!=0), the seed is ignored, and the
// generator is brought to the point as if the last random number generated was
// last_rn.
void init_random(int seed, int last_rn)
{
  if ((seed < 0) && (last_rn)) {
    prg_state[1] = last_rn;
    setstate((char*)prg_state);
  } else {
    if (seed < 0) { // make the seed from time ...
        struct timeval tv;
        struct timezone tzp;
        gettimeofday(&tv,&tzp);
        seed = (( tv.tv_sec & 0x000007FF ) * 1000000) + tv.tv_usec;
    }
    initstate((unsigned int)seed, (char*)prg_state, 8);
  }
}

// Returns the most recently generated random number (without affecting the
// sequence in any way)
int peek_random(void)
{
  return prg_state[1];
}

//                                                                           
// --------------------------  Local implementations ----------------------- 
//                                                                           


