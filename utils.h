/*
 Copyright (c) 2009-2011, Anton Belov, anton.belov@ucd.ie
 */

// ----------------------------------------------------------------------------
//
// utils.h - various utility datatypes and functions
//
// Anton Belov, antonb@cse.yorku.ca, April 2009 - ...
//
// ----------------------------------------------------------------------------

// Notes:
//

#ifndef _UTILS_H_
#define _UTILS_H_

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <sys/time.h>
#include <queue>
#include <vector>

//
// ----------------------- Data structures and types --------------------------
//

// Integer queue - this is an array-based queue for integers in the range
// (-max_size, max_size)\0. For every integer i in the interval, only one of i
// or -i may be stored in the queue; this queue should be used for various
// scheduling and propagation tasks; the main point is that each integer may
// appear in the queue only once; all operations, except comparison, are
// constant time (except comparison).
class int_queue {
public:
  // functionality
  void init(int max_size);  // initializes the queue
  void clear(void);         // clears the queue
  int push_back(int value); // queues a value
  void pop_front(void);     // removes the value at the front
  int front(void) const;    // returns the value at the front
  bool empty(void) const;   // true if the queue is empty
  bool operator==(const int_queue& q) const;
  int size(void) const;     // returns the size of the queue
  
// friend
  friend std::ostream& operator<<(std::ostream& out, const int_queue& q);
  
private:
  std::vector<int> _n;     // abs(_n[value]) is the index of the next in the
                           // queue; _max_size for last; sign is the sign of
                           // the value;
  int _max_size;           // the maximum size of the queue
  int _size;               // the size of the queue
  int _head;               // index of the head (-1 if empty)
  int _tail;               // index of the tail (== head if only one element)
  int _empty;              // value to use for "empty"
};

// Initializes the queue to support integers in the range [0,max_size)
inline void int_queue::init(int max_size)
{
  _max_size = max_size;
  _empty = max_size + 1;
  _n.resize(max_size, _empty); // -max_size is used for "empty"
  _size = 0;
  _head = -1;
}

// Clears the queue
inline void int_queue::clear(void)
{
  fill(_n.begin(), _n.end(), _empty);
  _size = 0;
  _head = -1;
}

// Queues the value; returns 0 if queued, 1 if already there, -1 if -value is
// already there; throws out_of_range if the value is invalid
inline int int_queue::push_back(int value)
{
  int idx = abs(value);
  if ((idx == 0) || (idx >= _max_size))
    throw std::out_of_range("int_queue::push_back() : value is out of range");
  // fast track when the queue is empty
  if (_head < 0) {
    _head = _tail = idx;
    _n[idx] = (value > 0) ? _max_size : -_max_size;
    _size = 1;
    return 0;
  } 
  if (_n[idx] == _empty) {
    // not there, queue it
    _n[_tail] = (_n[_tail] > 0) ? idx : -idx;
    _n[idx] = (value > 0) ? _max_size : -_max_size;
    _tail = idx;
    _size++;
    return 0;
  }
  // already there
  if (((_n[idx] > 0) && (value > 0)) || ((_n[idx] < 0) && (value < 0)))
    return 1;
  else
    return -1;
}

// Removes the value from the front of the queue; throws out_of_range if
// the queue is empty
inline void int_queue::pop_front(void)
{
  if (_head < 0)
    throw std::out_of_range("int_queue::pop_front() : empty queue");
  int old_head = _head;
  _head = abs(_n[_head]);
  if (_head == _max_size)
    _head = -1;
  _n[old_head] = _empty;
  _size--;
}

// Returns the value at the front of the queue; throws out_of_range if the
// queue is empty
inline int int_queue::front(void) const
{
  if (_head < 0)
    throw std::out_of_range("int_queue::front() : empty queue");
  return (_n[_head] > 0) ? _head : -_head;
}

// Returns true if the queue is empty
inline bool int_queue::empty(void) const
{
  return (_head < 0);
}

// Returns true if this queue has the same content as q
inline bool int_queue::operator==(const int_queue& q) const
{
  if ((_max_size == q._max_size) && (_head == q._head)) {
    if (_head < 0)
      return true;
    return (_tail == q._tail) && (_n == q._n);
  }
  return false;
}

// Returns the size of the queue
inline int int_queue::size(void) const
{
  return _size;
}

// Writes out the queue to the given output stream
inline std::ostream& operator<<(std::ostream& out, const int_queue& q)
{
  if (q._head < 0)
    out << "empty queue";
  else {
    for (int i = q._head; i != q._max_size; i = abs(q._n[i]))
      out << ((q._n[i] > 0) ? i : - i) << " ";
  }
  return out;
}

// Integer set - this is an array-based set for integers in the range [0,max_size).
// The iteration over set will return the elements in the order of their insertion.
// All operations are constant time (except operators == and []).
class int_set {
public:
  // a forward-only read-only iterator for the set; elements are accessed in
  // the order of their insertion
  class const_iterator {
  public:
    const_iterator(const int_set* owner, int idx) : _owner(owner), _idx(idx) {}
    int operator*(void) const; // dereferencing
    void operator++(void);     // increment
    bool operator==(const const_iterator& o) const; // equality
    bool operator!=(const const_iterator& o) const; // inequality
  private:
    const int_set* _owner;     // who owns
    int _idx;                  // where does it point
  };
  int_set(void) {}
  // initializes the set
  void init(int max_size);
  // clears the set
  void clear(void);
  // inserts a value; true if inserted, false if already there
  bool insert(int value);
  // erases a value; true if erased, false if wasn't there
  bool erase(int value);
  // 1 if value is there, 0 if not
  int count(int value);
  // returns the number of elements in the set
  int size(void) const;
  // returns true if set is empty
  bool empty(void) const;
  // returns true if two sets are equal (i.e. order doesn't matter)
  bool operator==(const int_set& s) const;
  // returns true if two sets are not equal (i.e. not ==)
  bool operator!=(const int_set& s) const;
  // returns i-th element (in the order of insertion) NOTE: very slow for
  // iteration, use int_set::const_iterator instead
  int operator[](int i) const;
  // returns iterator to the first element (in order of insertion)
  const_iterator begin(void) const;
  // returns iterator past the last element (in order of insertion)
  const_iterator end(void) const;
  
// friend
  friend std::ostream& operator<<(std::ostream& out, const int_set& q);
  
private:
  std::vector<int> _n;     // _n[value] is the index of the next in the set; _max_size for last
  std::vector<int> _p;     // _p[value] is the index of the previous in the set; -1 for first 
  int _max_size;           // the maximum size of the set
  int _size;               // the actual size of the set
  int _head;               // index of the head (-1 if empty)
  int _tail;               // index of the tail (== head if only one element) 
};

// Initializes the set
inline void int_set::init(int max_size)
{
  _n.resize(max_size, -1);
  _p.resize(max_size);
  _max_size = max_size;
  _size = 0;
}

// Clears the set
inline void int_set::clear(void)
{
  fill(_n.begin(), _n.end(), -1);
  _size = 0;
}

// Inserts a value; true if inserted, false if already there
inline bool int_set::insert(int value)
{
  if (_n[value] >= 0) // already there ...
    return false;
  if (_size == 0) { // first
    _head = _tail = value;
    _n[value] = _max_size;
    _p[value] = -1;
  } else { // insert into the end
    _n[value] = _max_size;
    _p[value] = _tail;
    _n[_tail] = value;
    _tail = value;
  }
  ++_size;
  return true;
}

// Erases a value; true if erased, false if wasn't there
inline bool int_set::erase(int value)
{
  if (_n[value] < 0)
    return false;
  if (_p[value] < 0) { // first, or first and last
    _head = _n[value];
    if (_head != _max_size)
      _p[_head] = -1;
  } else if (_n[value] == _max_size) { // last
    _tail = _p[value];
    _n[_tail] = _max_size;
  } else { // middle
    _n[_p[value]] = _n[value];
    _p[_n[value]] = _p[value];
  }
  _n[value] = -1;
  --_size;
  return true;
}

// Returns 1 if value is in the set, 0 if not
inline int int_set::count(int value) { return (_n[value] >= 0); }

// Returns the number of elements in the set
inline int int_set::size(void) const { return _size; }

// Returns true if set is empty
inline bool int_set::empty(void) const { return _size == 0; }   

// Returns true is this set is exactly like s (order doesn't matter)
inline bool int_set::operator==(const int_set& s) const
{
  if ((_max_size == s._max_size) && (_size == s._size)) {
    if (_size == 0)
      return true;
    // if non-empty, we only care about the content
    for (int i = 0; i < _max_size; i++)
      if (((_n[i] >= 0) && (s._n[i] < 0)) ||
          ((_n[i] < 0) && (s._n[i] >= 0)))
        return false;
    return true;
  }
  return false;
}

// Returns true is this set is exactly like s (order doesn't matter)
inline bool int_set::operator!=(const int_set& s) const
{
  return !(*this == s);
}

// Returns i-th element (in the order of insertion). NOTE: this is very slow,
// use int_set::const_iterator instead
inline int int_set::operator[](int i) const
{
  if (i < 0 || i >= _size)
    throw std::out_of_range("int_set::operator[] : index out of range");
  for (int j = _head; j != _max_size; j = _n[j])
    if (--i < 0)
      return j;
  assert(false); // shouldn't be here, unless _size is wrong  
  return -1;
}

// Returns the const_iterator to the begining of the set; will be equal to the
// end() if empty
inline int_set::const_iterator int_set::begin(void) const
{
  // if empty, point to the end
  return int_set::const_iterator(this, (_size == 0) ? _max_size : _head);
}

// Returns the const_iterator to past the end
inline int_set::const_iterator int_set::end(void) const
{
  return int_set::const_iterator(this, _max_size);
}

// Dereferencing operator for int_set::const_iterator
inline int int_set::const_iterator::operator*(void) const
{
  if (_idx == _owner->_max_size)
    throw std::out_of_range("int_set::const_iterator::operator* : iterator points to the end");
  return _idx;
}

// Increment operator for int_set::const_iterator; if the iterator points to
// the end, nothing will happen
inline void int_set::const_iterator::operator++(void)
{
  // increment will do nothing if already at the end
  if (_idx != _owner->_max_size)
    _idx = _owner->_n[_idx];
}

// Equality operator for int_set::const_iterator
inline bool int_set::const_iterator::operator==(const int_set::const_iterator& o) const
{
  return (_owner == o._owner) && (_idx == o._idx);
}

// Inequality for int_set::const_iterator
inline bool int_set::const_iterator::operator!=(const int_set::const_iterator& o) const
{
  return !(*this == o);
}

// Writes out the set to the specified output stream
inline std::ostream& operator<<(std::ostream& out, const int_set& s)
{
  if (s._size == 0)
    out << "empty set";
  else
    for (int i = s._head; i != s._max_size; i = s._n[i])
      out << i << " ";
  return out;
}


// Integer priority queue - this is a priority queue for integers in the range
// [0,max_size) that does not allow duplicates. The entries are sorted in the
// the order specified by the template parameter Cmp is a function used for
// ordering the queue:
//   std::less<int> (the default) will keep the queue in the decreasing order,
//   that is top() returns the largest entry.
//   std::greater<int> will keep the queue in the increasing order, that is
//   top() will returns the smallest entry.
// This queue can be used for various scheduling and propagation tasks; top()
// is O(1), pop() is O(n) worst/ O(log(n)) amortized; push() is O(log(n)) worst.
// Note that the GNU C++ library has other options that could be better.
template< typename _Cmp = std::less<int> > class int_pqueue {
public:
  // lifecycle
  int_pqueue() : _cmp(), _q(_cmp) {}
  // functionality
  void init(int max_size);  // initializes the queue
  void clear(void);         // clears the queue
  bool push(int value);     // queues a value; true if queued, false if already there
  void pop(void);           // removes the value at the front; throws out_of_range if empty
  int top(void) const;      // returns the value at the front; throws out_of_range if empty
  bool insert(int value);   // inserts the value - same as push
  bool erase(int value);    // removes the value (from the middle too)
  bool empty(void) const;   // true if the queue is empty
  int size(void) const;     // returns the size of the queue
  bool operator==(const int_pqueue& q) const;
  // compatibility i/f - for easy switching between queue and pqueue
  bool push_back(int value) { return push(value); }
  void pop_front(void) { pop(); }
  int front(void) { return top(); }
  // access to the instance of comparator
  _Cmp& comparator() { return _cmp; }
  
// friend
  // Note: the code is here, due to C++ template/friend combination wierdness,
  // see [35.16] in C++ FAQ Lite
  friend std::ostream& operator<<(std::ostream& out, const int_pqueue<_Cmp>& q)
  {
    if (q._q.empty())
      out << "empty queue";
    else {
      for (int i = 0; i < (int)q._n.size(); i++)
        if (q._n[i])
          out << i << " ";
    }
    return out;
  }
  
private:
  _Cmp _cmp; // the comparator
  std::priority_queue<int, std::vector<int>, _Cmp > _q;
  std::vector<unsigned char> _n;  // _n[value] is 1 if the value is in the queue
                                  // 0 if the value is not in the queue
                                  // 2 if the value is not in the queue, but
                                  //   still in the underlying priority queue
  int _size;                      // only non-ghosts
};


// Initializes the queue to support integers in the range [0,max_size)
template<typename _Cmp>
void int_pqueue<_Cmp>::init(int max_size)
{
  _n.resize(max_size, 0);
  _size = 0;
}

// Clears the queue
template<typename _Cmp>
void int_pqueue<_Cmp>::clear(void)
{
  fill(_n.begin(), _n.end(), 0);
  while (!_q.empty())
    _q.pop();
  _size = 0;
}

// Queues the value; false if the value already in the queue; true if new and queued
template<typename _Cmp>
bool int_pqueue<_Cmp>::push(int value)
{
  if (_n[value] == 1) // already there
    return false;
  if (_n[value] == 0) { // its not in the physical queue, so need to queue it
    _q.push(value);
  }
  _size++;
  // now it will be considered to be in the queue
  _n[value] = 1;
  return true;
}

// Removes the value from the front of the queue
template<typename _Cmp>
void int_pqueue<_Cmp>::pop(void)
{
  _n[_q.top()] = 0; // note: this will remove the "ghosts"
  _q.pop();
  _size--;
}

// Returns the value at the front of the queue
template<typename _Cmp>
int int_pqueue<_Cmp>::top(void) const
{
  int top = _q.top();
  // we use this opportunity to remove the ghosts from the top of the queue
  assert(_n[top] > 0);
  while (_n[top] == 2) { // ghost
    const_cast<std::priority_queue<int, std::vector<int>, _Cmp >&>(_q).pop();
    const_cast<std::vector<unsigned char>&>(_n)[top] = 0;
    top = _q.top();
    assert(_n[top] > 0);
  }
  return top;
}

// Insert - same as push
template<typename _Cmp>
bool int_pqueue<_Cmp>::insert(int value)
{
  return push(value);
}

// Removes the value from anywhere in the queue; true if removed; false if
// wasn't there ...
template<typename _Cmp>
bool int_pqueue<_Cmp>::erase(int value)
{
  if (_n[value] != 1)
    return false;
  _n[value] = 2; // its a ghost now ...
  _size--;
  return true;
}

// Returns true if the queue is empty
template<typename _Cmp>
bool int_pqueue<_Cmp>::empty(void) const
{
  return size() == 0;
}

// Returns the size of the queue
template<typename _Cmp>
int int_pqueue<_Cmp>::size(void) const
{
  return _size;
}

// Returns true if this queue has the same content as q
template<typename _Cmp>
bool int_pqueue<_Cmp>::operator==(const int_pqueue<_Cmp>& q) const
{
  return (_n == q._n);
}

//
// ------------------------  Global variable declarations ---------------------
//

//
// ----------------------------  Inline functions -----------------------------
//

// Tosses a "coin": returns 0 or 1 with probability 0.5.
inline int random_toss() { return (rand() > (RAND_MAX/2)) ? 1 : 0; }

// Returns a random integer from [0, limit]
inline int random_int(int limit) { return (int)((limit+1.0)*rand()/(RAND_MAX+1.0)); }

// Returns a random double from [0, 1)
inline double random_double() { return ((double)rand()/(RAND_MAX+1.0)); }

// Returns the sign (-1 if negative, 1 if positive, 0 if 0)
inline int sign(int n) { return (n < 0) ? -1 : ((n > 0) ? 1 : 0); }

//
// ----------------------------  Outline functions -----------------------------
//

// Initializes random generator. If seed>=0, then the generator is initialized
// with this value of seed, and the last_rn parameter is ignored. If seed<0,
// and then if last_rn=0 the current time is used to initialize the generator,
// otherwise (i.e.e seed<0 and last_rn!=0), the seed is ignored, and the
// generator is brought to the point as if the last random number generated was
// last_rn.
void init_random(int seed, int last_rn = 0);

// Returns the most recently generated random number (without affecting the
// sequence in any way)
int peek_random(void);


#endif // _UTILS_H_
